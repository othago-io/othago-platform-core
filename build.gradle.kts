plugins {
    kotlin("multiplatform") version "1.4.32"
    kotlin("plugin.serialization") version "1.4.32"
    kotlin("plugin.spring") version "1.4.32"
    kotlin("plugin.jpa") version "1.4.32"
    id("maven-publish")
}

group = "io.othago"
version = "1.0-SNAPSHOT"

val kotlinxSerializationVersion: String by project
val kotlinxCoroutinesVersion: String by project
val kotlinxCollectionsImmutableVersion: String by project
val kotlinxDatetime: String by project
val uuidVersion: String by project
val kotlinVersion: String by project
val kotlinWrappersSuffix: String by project
val springBootVersion: String by project
val undertowVersion: String by project
val junitJupiterEngineVersion: String by project
val keycloakVersion: String by project
val kotlinLoggingVersion: String by project
val flywayVersion: String by project
val h2Version: String by project
val pFreeCompilerArgs: String by project

repositories {
    mavenLocal()
    mavenCentral()
    maven("https://repo.spring.io/milestone")
    maven("https://repo.spring.io/snapshot")
    maven("https://maven.pkg.jetbrains.space/public/p/kotlinx-html/maven")
    maven("https://maven.pkg.jetbrains.space/kotlin/p/kotlin/kotlin-js-wrappers")
    jcenter()
}

publishing {
    repositories {
        mavenLocal {}
    }
    publications {
        create<MavenPublication>("maven") {
            from(components["kotlin"])
            pom {
                name.set("Othago Platform Core Api")
                description.set("Platform Api for building solutions.")
                url.set("http://othago.io/platform-core")
                licenses {
                    license {
                        name.set("The Apache License, Version 2.0")
                        url.set("http://www.apache.org/licenses/LICENSE-2.0.txt")
                    }
                }
            }
        }
    }
}

kotlin {
    jvm {
        compilations.all {
            kotlinOptions.jvmTarget = "15"
            kotlinOptions.freeCompilerArgs = listOf(pFreeCompilerArgs)
        }
        testRuns["test"].executionTask.configure {
            useJUnit()
        }
    }
    js {
        useCommonJs()
        browser {
            commonWebpackConfig {
                cssSupport.enabled = true
            }
            testTask {
                testLogging {
                    showExceptions = true
                    exceptionFormat = org.gradle.api.tasks.testing.logging.TestExceptionFormat.FULL
                    showCauses = true
                    showStackTraces = true
                }
                useKarma {
                    useChromium()
                }
            }
        }
    }

    sourceSets {
        val commonMain by getting {
            dependencies {
                api(kotlin("stdlib-common"))
                api("org.jetbrains.kotlinx:kotlinx-serialization-core:$kotlinxSerializationVersion")
                api("org.jetbrains.kotlinx:kotlinx-serialization-json:$kotlinxSerializationVersion")
                api("org.jetbrains.kotlinx:kotlinx-collections-immutable:$kotlinxCollectionsImmutableVersion")
                api("org.jetbrains.kotlinx:kotlinx-datetime:$kotlinxDatetime")
                api("com.benasher44:uuid:$uuidVersion")
            }
        }
        val commonTest by getting {
            dependencies {
                api(kotlin("test-common"))
                api(kotlin("test-annotations-common"))
            }
        }
        val jvmMain by getting {
            dependencies {
                api(kotlin("stdlib-jdk8"))
                api("org.jetbrains.kotlinx:kotlinx-serialization-core:$kotlinxSerializationVersion")
                api("org.jetbrains.kotlinx:kotlinx-serialization-json:$kotlinxSerializationVersion")
                api("org.jetbrains.kotlinx:kotlinx-collections-immutable:$kotlinxCollectionsImmutableVersion")
                api("org.jetbrains.kotlinx:kotlinx-datetime:$kotlinxDatetime")
                api("com.benasher44:uuid:$uuidVersion")

                api("org.jetbrains.kotlin:kotlin-reflect:$kotlinVersion")
                api("io.github.microutils:kotlin-logging:$kotlinLoggingVersion")
                api("org.springframework.boot:spring-boot-starter-validation:$springBootVersion")
                api("org.springframework.boot:spring-boot-starter-data-jpa:$springBootVersion")
                api("org.springframework.boot:spring-boot-starter-web:$springBootVersion")
                api("io.undertow:undertow-servlet:$undertowVersion")
                api("org.keycloak:keycloak-adapter-core:$keycloakVersion")
                api("org.keycloak:keycloak-adapter-spi:$keycloakVersion")

                runtimeOnly("org.flywaydb:flyway-core:$flywayVersion")
                runtimeOnly("com.h2database:h2:$h2Version")

                api("org.springframework.boot:spring-boot-starter-test:$springBootVersion")
            }
        }
        val jvmTest by getting {
            dependencies {
                api(kotlin("stdlib-jdk8"))
                api(kotlin("test"))
                api(kotlin("test-junit"))
                api("org.junit.jupiter:junit-jupiter-engine:$junitJupiterEngineVersion")
                api("org.springframework.boot:spring-boot-starter-test:$springBootVersion")
            }
        }
        val jsMain by getting {
            dependencies {
                api(kotlin("stdlib-js"))
                api("org.jetbrains.kotlinx:kotlinx-coroutines-core-js:$kotlinxCoroutinesVersion")
            }
        }
        val jsTest by getting {
            dependencies {
                api(kotlin("test-js"))
            }
        }
    }
}

tasks.withType<org.gradle.jvm.tasks.Jar>().all {
    from(project.projectDir) {
        include("LICENSE.txt", "NOTICE.txt", "README.md")
        into("META-INF")
    }
}

fun org.jetbrains.kotlin.gradle.targets.js.dsl.KotlinJsTargetDsl.workaroundKT38495() {
    // workaround duplicate target file (KT-38495)
    (tasks[artifactsTaskName] as AbstractArchiveTask).apply {
        exclude("lib")
    }
}
