# PREREQUIST

## INSTALL JDK15

```
sudo apt-get install openjdk-15-jdk
```

> File | Project Structure | SDKs

15 (openjdk15)

## GRADLE

Install gradle 7.0
```
sudo add-apt-repository ppa:cwchien/gradle
sudo apt-get update
sudo apt upgrade gradle
gradle --version
```

### Resolving issues with older gradle

Uninstall previous ones
```
gradle --version
sudo apt remove gradle-6.7
sudo apt remove gradle-6.8
sudo apt remove gradle-6.8.3 
```

Clear gradle-wrapper cache /home/userx/.gradle
```
cd
rm -rf .gradle
```

## KOTLIN SETUP

> File | Settings | Build, Execution, Deployment | Compiler | Kotlin Compiler

Target JVM Version: 15

> File | Project Structure | Project

Project SDK: 15

Project language level: SDK default 15

## SETUP COPYRIGHT

> File | Settings | Editor | Copyright | Copyright Profiles

Name: apache2.0

Copyright Text: 
```
Copyright 2015-2021 the original author or authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```

> File | Settings | Editor | Copyright

Default copyright project: apache2.0

Scope (+): All / apache2.0

## SETUP AUTO-FORMAT

> File | Settings | Editor | Code Style

Line separator: Unix \n

Hard wrap at: 180

> File | Settings | Editor | Code Style | Kotlin

Cog | Import schema: idea-kotlin-code-style.xml

## SETUP AUTO-FORMAT ON COMMIT

Commit Tab (Alt+0) | Cog under Default Changelist (near Amend)

Select: all without copyright (Reformat code, Rearrange code, Optimize imports, Perform code analysis, Check TODO, Cleanup)

## DIAGRAMS.NET PLUGIN

> File | Settings | Plugins

Diagrams.net Integration (find and install)
