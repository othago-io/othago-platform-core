CREATE TABLE audit_entries
(
    id                UUID,
    created_at        TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    created_by_id     UUID      NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',

    tx_id             TEXT,

    root_action_name  TEXT,
    exec_action_name  TEXT,

    data_item_proto   INTEGER,
    data_item_type    TEXT,
    data_item_id      UUID,
    data_item_version BIGINT             DEFAULT 0,

    change_type       INTEGER,
    change_details    JSON      NOT NULL DEFAULT '[]',

    PRIMARY KEY (id)
);

commit;
