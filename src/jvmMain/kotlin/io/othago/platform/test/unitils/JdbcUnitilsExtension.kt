/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.test.unitils

import io.othago.platform.common.lang.isNotNull
import kotlinx.serialization.json.*
import org.junit.jupiter.api.extension.BeforeAllCallback
import org.junit.jupiter.api.extension.BeforeEachCallback
import org.junit.jupiter.api.extension.ExtensionContext
import org.slf4j.LoggerFactory
import org.springframework.core.io.ClassPathResource
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.test.context.junit.jupiter.SpringExtension

/* TODO cleanup strategy / table list */
annotation class JdbcTestData(val value: String)

class JdbcUnitilsExtension : BeforeAllCallback, BeforeEachCallback {
    companion object {
        private val log = LoggerFactory.getLogger(this::class.qualifiedName)
    }


    private val json: Json = Json { isLenient = true; ignoreUnknownKeys = true }

    private fun retJdbcTemplate(context: ExtensionContext) = SpringExtension.getApplicationContext(context).getBeansOfType(JdbcTemplate::class.java).values.first()!!
    private fun retTestDataLocation(context: ExtensionContext) = context.element.get().getDeclaredAnnotation(JdbcTestData::class.java)

    override fun beforeAll(context: ExtensionContext?) = loadData(retTestDataLocation(context!!), retJdbcTemplate(context))
    override fun beforeEach(context: ExtensionContext?) = loadData(retTestDataLocation(context!!), retJdbcTemplate(context))

    private fun loadData(jdbcTestDataAnnotation: JdbcTestData?, jdbcTemplate: JdbcTemplate) {
        if (jdbcTestDataAnnotation.isNotNull()) {
            log.debug("triggering loadData for ${jdbcTestDataAnnotation!!.value}")
            val rootTestDataSetElement = json.parseToJsonElement(ClassPathResource(jdbcTestDataAnnotation!!.value).file.readText())
            rootTestDataSetElement.jsonObject.entries.forEach { e ->
                log.info("loading ${e.key} with ${e.value.jsonArray.size} entries")
                e.value.jsonArray.forEachIndexed { index,tuple ->
                    val fields = tuple.jsonObject.entries
                    val insert = "INSERT INTO ${e.key} (${fields.joinToString { it.key }}) VALUES (${fields.joinToString { convertStrVal(it.value.jsonPrimitive) }})"
                    log.info("[$index] inserting $insert")
                    jdbcTemplate.execute(insert)
                }
                log.info("loaded: ${jdbcTemplate.queryForObject("SELECT COUNT(*) FROM ${e.key}", Int::class.java)} ${jdbcTemplate.queryForList("SELECT * FROM ${e.key}")}")
            }
        }
    }

    private fun convertStrVal(value: JsonPrimitive): String = if (value.isString) "'${value.content}'" else value.content

}