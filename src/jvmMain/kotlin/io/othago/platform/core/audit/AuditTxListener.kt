/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.audit

import io.othago.platform.core.api.cqrs.*
import io.othago.platform.core.cqrs.ProcessingContext
import io.othago.platform.core.transaction.ITransactionListener

class AuditTxListener(val auditService: IAuditService) : ITransactionListener {

    override fun <CTX : ProcessingContext, E : IPlatformEvent, RR : EventResult> beforeCommit(ctx: CTX, event: E, res: RR) {
        if (res is CommandResult<*>) {
            res.payload.filterIsInstance<DataItemRE>().forEach { re ->
                auditService.captureDataItemRE(
                    ctx.secCtx.userCtx,
                    ctx.txCtx.txId,
                    event.retActionName(), //root entry event
                    re
                )
            }
        }
    }

    override fun <CTX : ProcessingContext, E : IPlatformEvent, RR : EventResult> afterCommit(ctx: CTX, event: E, res: RR) {}

    override fun <CTX : ProcessingContext, E : IPlatformEvent> beforeRollback(ctx: CTX, event: E, th: Throwable) {}
    override fun <CTX : ProcessingContext, E : IPlatformEvent> afterRollback(ctx: CTX, event: E, th: Throwable) {}
}