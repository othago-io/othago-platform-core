/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.audit

import io.othago.platform.core.api.cqrs.DeltaChange
import io.othago.platform.core.api.exception.BusinessException
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive

private fun listOfDeltaChanges(prefixKey: String, intial: JsonObject?, saved: JsonObject?): List<DeltaChange> =
// TODO embedded objects - deeper hierarchies
    // TODO different types of fields then String
    (intial?.keys ?: listOf()).union(saved?.keys ?: listOf())
        .map { key ->
            val oldv = intial?.get(key)
            val newv = saved?.get(key)

            val oldvjp = oldv as? JsonPrimitive
            val newvjp = newv as? JsonPrimitive

            val oldvjo = oldv as? JsonObject
            val newvjo = newv as? JsonObject

            if ((oldv == null || oldvjp != null) && (newv == null || newvjp != null)) { // primitives
                listOf(
                    DeltaChange(
                        "$prefixKey$key",
                        oldvjp?.content,
                        newvjp?.content
                    )
                )
            } else if ((oldv == null || oldvjo != null) && (newv == null || newvjo != null)) { // objects
                listOfDeltaChanges("$prefixKey$key.", oldvjo, newvjo)
            } else {
                throw BusinessException(msg = "Unrecognized combination of attributes during delta calculation ($oldv, $newv).")
            }
        }.flatten<DeltaChange>()

fun JsonObject.delta(initial: JsonObject?): List<DeltaChange> = listOfDeltaChanges("", initial, this)
    .let { println("=== unfiltered $it"); it }
    .filter {
        // filter out empty ones
        !(it.initialValue == null && it.updatedValue == null)
                // and no changes
                && (it.initialValue != it.updatedValue)
    }
    .let { println("=== filtered $it"); it }