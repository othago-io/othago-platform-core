/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.audit

import com.benasher44.uuid.Uuid
import io.othago.platform.core.api.audit.AuditEntry
import io.othago.platform.core.api.audit.GetEntriesForDataItemQE
import io.othago.platform.core.api.cqrs.PageResult
import io.othago.platform.core.api.cqrs.QueryResult
import io.othago.platform.core.deploy.PlatformEnvironment
import io.othago.platform.core.integration.spring.data.toPageRequest
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.web.PageableDefault
import org.springframework.data.web.SortDefault
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/audit")
class AuditController {
    @Autowired
    lateinit var env: PlatformEnvironment

    @GetMapping("/v0/entriesForDateItem/{type}/{id}")
    fun entriesForDateItem(
        @PathVariable("type") dataItemType: String,
        @PathVariable("id") dataItemId: Uuid,
        @PageableDefault(page = 0, size = 10)
        @SortDefault.SortDefaults(SortDefault(sort = ["createdAt"], direction = Sort.Direction.ASC)) page: Pageable
    ): QueryResult<PageResult<AuditEntry>> = env.queryEntryPoint.executeQuery(GetEntriesForDataItemQE(dataItemType, dataItemId, page.toPageRequest()))

}