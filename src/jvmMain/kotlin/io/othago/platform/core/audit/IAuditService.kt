/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.audit

import com.benasher44.uuid.Uuid
import io.othago.platform.core.api.audit.AuditEntry
import io.othago.platform.core.api.cqrs.DataItemProto
import io.othago.platform.core.api.cqrs.DataItemRE
import io.othago.platform.core.api.cqrs.PageRequest
import io.othago.platform.core.security.UserContext
import org.springframework.data.domain.Page

interface IAuditService {

    fun captureDataItemRE(
        user: UserContext,
        txId: String,
        rootActionName: String, // entry command | query business event name
        dataItemRE: DataItemRE
    )

    fun getEntriesForDataItem(
        dataItemType: String,
        dataItemid: Uuid,
        pageRequest: PageRequest?,
        dataItemProto: DataItemProto
    ): Page<AuditEntry>

}