/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.integration.spring

import io.othago.platform.core.api.cqrs.*
import io.othago.platform.core.deploy.PlatformEnvironment
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("/api/v1")
class GenericRouter(val env: PlatformEnvironment) {
    @PostMapping("/command", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun command(@RequestBody event: CommandEvent): CommandResult<RepositoryEvent> = env.commandEntryPoint.executeCommand(event)

    @PostMapping("/query", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun query(@RequestBody event: QueryEvent): QueryResult<IResultObject> = env.queryEntryPoint.executeQuery(event)
}
