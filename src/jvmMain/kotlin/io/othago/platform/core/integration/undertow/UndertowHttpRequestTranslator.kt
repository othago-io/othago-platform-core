package io.othago.platform.core.integration.undertow

import io.othago.platform.core.integration.spring.web.IHttpRequestTranslator
import io.undertow.servlet.spec.HttpServletRequestImpl
import org.springframework.web.context.request.ServletWebRequest
import org.springframework.web.context.request.WebRequest
import org.springframework.web.multipart.support.DefaultMultipartHttpServletRequest
import javax.servlet.http.HttpServletRequest

class UndertowHttpRequestTranslator : IHttpRequestTranslator {

    override fun getReferer(req: HttpServletRequest): String {
        if (req is HttpServletRequestImpl) return req.originalRequestURI
        return if (!req.getHeader("referer").isNullOrBlank()) {
            req.getHeader("referer")
        } else ""
    }

    override fun getReferer(req: WebRequest): String {
        if (req is ServletWebRequest) {
            return when (val nativeReq = req.nativeRequest) {
                is HttpServletRequestImpl -> nativeReq.originalRequestURI
                is DefaultMultipartHttpServletRequest -> nativeReq.requestURI
                else -> req.request.requestURI
            }
        }
        return ""
    }
}