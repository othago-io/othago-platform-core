package io.othago.platform.core.integration.spring.web

import org.springframework.web.context.request.WebRequest
import javax.servlet.http.HttpServletRequest

interface IHttpRequestTranslator {
    fun getReferer(req: HttpServletRequest): String
    fun getReferer(req: WebRequest): String
}