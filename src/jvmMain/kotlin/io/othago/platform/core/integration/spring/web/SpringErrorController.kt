/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.integration.spring.web

import io.othago.platform.common.exception.InfrastructureValidationAssert
import io.othago.platform.common.lang.isNull
import io.othago.platform.core.api.cqrs.ErrorResult
import org.springframework.boot.web.servlet.error.ErrorController
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@RestController
class SpringErrorController(
    val httpReqTranslator: IHttpRequestTranslator,
    val errorResponseMappers: List<IErrorResponseMapper> = emptyList()
) : ErrorController {

    override fun getErrorPath(): String = "/error"

    @RequestMapping("/error")
    fun getErrorPage(request: HttpServletRequest, response: HttpServletResponse): ResponseEntity<ErrorResult> {
        val referer = httpReqTranslator.getReferer(request)
        InfrastructureValidationAssert.assertNotBlank(referer, "Referer for error page handling loop should not be empty")

        var errorResponse = errorResponseMappers.map { it.mapError(referer, request, response) }.firstOrNull()
        if (errorResponse.isNull()) {
            errorResponse = SpringErrorResponseMapper.mapError(referer, request, response)
        }
        return errorResponse!!
    }

}