/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.integration.spring.web

import io.othago.platform.common.lang.isNotNull
import io.othago.platform.core.api.cqrs.ErrorItem
import io.othago.platform.core.api.cqrs.ErrorResult
import io.othago.platform.core.api.cqrs.toResultObject
import io.othago.platform.core.api.exception.IPlatformExceptionCode
import io.othago.platform.core.api.exception.PlatformException
import io.othago.platform.core.api.exception.PlatformExceptionCode
import org.apache.catalina.connector.RequestFacade
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.context.request.ServletWebRequest
import org.springframework.web.context.request.WebRequest
import org.springframework.web.multipart.support.DefaultMultipartHttpServletRequest
import java.net.URI
import java.net.URISyntaxException
import javax.servlet.ServletRequestWrapper
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

//TODO parametrize -> auth error page shown to user
const val AUTH_ERROR_REDIRECT = "/authError"

object ErrorResponseHelper {

    fun buildErrorResponse(status: Int, ex: PlatformException): ResponseEntity<ErrorResult> {
        return ResponseEntity.status(status).body(ex.toErrorResult())
    }

    fun buildErrorResponse(status: Int, code: IPlatformExceptionCode, msg: String): ResponseEntity<ErrorResult> {
        return ResponseEntity.status(status).body(ErrorResult(ErrorItem(code.toResultObject(), msg)))
    }

    fun handleAuthError(referer: String, resp: HttpServletResponse): ResponseEntity<ErrorResult> {
        val authHeaderValue = resp.getHeader("WWW-Authenticate")
        return if (authHeaderValue.isNotNull()) {
            handleError(referer, buildErrorResponse(resp.status, PlatformExceptionCode.INF_AUTH_UNAUTHORIZED, "$authHeaderValue"))
        } else {
            handleError(referer, buildErrorResponse(resp.status, PlatformExceptionCode.INF_AUTH_GENERIC, "$authHeaderValue"))
        }
    }

    fun handleServletError(referer: String, req: HttpServletRequest, resp: HttpServletResponse): ResponseEntity<ErrorResult> {
        val servletErrorHeaderValue = req.getAttribute("javax.servlet.error.message")
        return if (servletErrorHeaderValue.isNotNull()) {
            handleError(referer, buildErrorResponse(resp.status, PlatformExceptionCode.INF_SERVLET_GENERIC, "$servletErrorHeaderValue"))
        } else {
            handleError(referer, buildErrorResponse(resp.status, PlatformExceptionCode.PLF_GENERIC, HttpStatus.valueOf(resp.status).reasonPhrase))
        }
    }

    fun handleError(referer: String, re: ResponseEntity<ErrorResult>): ResponseEntity<ErrorResult> {
        return if (isAuthLoginFlow(referer)) { //redirect to auth error page
            try {
                ResponseEntity.status(HttpStatus.MOVED_PERMANENTLY).location(URI(AUTH_ERROR_REDIRECT)).body(re.body)
            } catch (ex: URISyntaxException) {
                ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(re.body)
            }
        } else re
    }

    private fun isAuthLoginFlow(url: String): Boolean {
        //TODO pattern + app props
        return url.endsWith("/auth/v1/login")
    }

}