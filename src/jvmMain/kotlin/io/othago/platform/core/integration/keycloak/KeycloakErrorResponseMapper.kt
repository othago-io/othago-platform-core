/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.integration.keycloak

import io.othago.platform.common.lang.isNotNull
import io.othago.platform.core.api.cqrs.ErrorResult
import io.othago.platform.core.api.exception.IPlatformExceptionCode
import io.othago.platform.core.api.exception.PlatformExceptionCode
import io.othago.platform.core.integration.spring.web.ErrorResponseHelper
import io.othago.platform.core.integration.spring.web.ErrorResponseHelper.buildErrorResponse
import io.othago.platform.core.integration.spring.web.ErrorResponseHelper.handleError
import io.othago.platform.core.integration.spring.web.IErrorResponseExceptionMapper
import io.othago.platform.core.integration.spring.web.IErrorResponseMapper
import org.keycloak.adapters.OIDCAuthenticationError
import org.keycloak.adapters.ServerRequest
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class KeycloakErrorResponseMapper : IErrorResponseMapper {

    override fun mapError(
        referer: String,
        request: HttpServletRequest,
        response: HttpServletResponse
    ): ResponseEntity<ErrorResult>? {

        if (response.status == HttpStatus.UNAUTHORIZED.value()) {
            val keycloakError = request.getAttribute("org.keycloak.adapters.spi.AuthenticationError")
            if (keycloakError.isNotNull() && keycloakError is OIDCAuthenticationError) {

                if (keycloakError.reason == OIDCAuthenticationError.Reason.NO_BEARER_TOKEN) {
                    //NO JWT
                    return handleError(
                        referer, buildErrorResponse(
                            response.status,
                            PlatformExceptionCode.INF_AUTH_JWT_NO_ACCESS_TOKEN,
                            "No JWT Access Token"
                        )
                    )
                } else if (keycloakError.reason == OIDCAuthenticationError.Reason.INVALID_TOKEN &&
                    keycloakError.description.isNotBlank() && keycloakError.description == "Token is not active") {

                    //JWT expired
                    return handleError(
                        referer, buildErrorResponse(
                            response.status,
                            PlatformExceptionCode.INF_AUTH_JWT_ACCESS_TOKEN_EXPIRED,
                            "Access Token Expired"
                        )
                    )
                }else {
                    return handleError(
                        referer, buildErrorResponse(
                            response.status,
                            KeycloakExceptionCode.INF_AUTH_KEYCLOAK_GENERIC,
                            keycloakError.reason.name
                        )
                    )
                }
            }
        }
        return null
    }
}

@Component
class KeycloakErrorResponseExceptionMapper : IErrorResponseExceptionMapper {
    override fun mapException(referer: String, ex: Throwable): ResponseEntity<ErrorResult>? {
        if (ex.cause is ServerRequest.HttpFailure) {
            val keycloakException = ex.cause as ServerRequest.HttpFailure
            var code : IPlatformExceptionCode = KeycloakExceptionCode.INF_AUTH_KEYCLOAK_GENERIC
            var msg: String = keycloakException.error

            if (msg.contains("Refresh token expired")) {
                code = PlatformExceptionCode.INF_AUTH_JWT_REFRESH_TOKEN_EXPIRED
                msg = "JWT refresh token expired"
            }

            val status = HttpStatus.valueOf(keycloakException.status)
            return handleError(referer, buildErrorResponse(status.value(), code, msg))
        }
        return null
    }
}