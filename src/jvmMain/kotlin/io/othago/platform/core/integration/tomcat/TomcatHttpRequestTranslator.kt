package io.othago.platform.core.integration.tomcat

import io.othago.platform.core.integration.spring.web.IHttpRequestTranslator
import org.apache.catalina.connector.RequestFacade
import org.springframework.web.context.request.ServletWebRequest
import org.springframework.web.context.request.WebRequest
import org.springframework.web.multipart.support.DefaultMultipartHttpServletRequest
import javax.servlet.ServletRequestWrapper
import javax.servlet.http.HttpServletRequest

class TomcatHttpRequestTranslator : IHttpRequestTranslator {

    override fun getReferer(req: HttpServletRequest): String {
        if (req is ServletRequestWrapper) {
            if (req.request is RequestFacade) return (req.request as RequestFacade).requestURI
        }
        return if (!req.getHeader("referer").isNullOrBlank()) {
            req.getHeader("referer")
        } else ""
    }

    override fun getReferer(req: WebRequest): String {
        if (req is ServletWebRequest) {
            return when (val nativeReq = req.nativeRequest) {
                is RequestFacade -> nativeReq.requestURI
                is DefaultMultipartHttpServletRequest -> nativeReq.requestURI
                else -> req.request.requestURI
            }
        }
        return ""
    }
}