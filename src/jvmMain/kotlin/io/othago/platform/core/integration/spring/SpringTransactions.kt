/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.integration.spring

import com.benasher44.uuid.Uuid
import io.othago.platform.common.exception.InfrastructureConfigurationAssert
import io.othago.platform.core.api.exception.InfrastructureConfigurationException
import io.othago.platform.core.api.exception.InfrastructureCoreException
import io.othago.platform.core.api.exception.PlatformExceptionCode
import io.othago.platform.core.transaction.IPlatformTxManager
import io.othago.platform.core.transaction.ITransactionContextProvider
import io.othago.platform.core.transaction.ITransactionStatus
import io.othago.platform.core.transaction.TransactionContext
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.transaction.PlatformTransactionManager
import org.springframework.transaction.TransactionDefinition
import org.springframework.transaction.TransactionDefinition.PROPAGATION_NEVER
import org.springframework.transaction.TransactionDefinition.PROPAGATION_REQUIRES_NEW
import org.springframework.transaction.TransactionStatus
import org.springframework.transaction.support.AbstractPlatformTransactionManager


/* Spring Tx manager adapter */
@Component
class SpringTxContextProvider : ITransactionContextProvider {

    companion object {
        private val log = LoggerFactory.getLogger(this::class.qualifiedName!!)
    }

    @Autowired
    lateinit var txManager: ISpringTxManagerAdapter

    override fun provideTransactionContext(): TransactionContext {
        val txId = Uuid.randomUUID().toString() //TODO wrap with platform sequence + platform independent generator, if monotonic then stateful
        val tc = TransactionContext(txId, txManager, txManager.getTransaction())
        log.info("[$tc] provideTransactionContext")
        return tc
    }

}

interface ISpringTxManagerAdapter : IPlatformTxManager

@Component
class SpringTxManagerAdapter(val txManagerName: String = "DefaultSpringTxProvider") : ISpringTxManagerAdapter {

    companion object {
        private val log = LoggerFactory.getLogger(this::class.qualifiedName!!)

        private val txDefReqNew = object : TransactionDefinition {
            override fun getPropagationBehavior(): Int = PROPAGATION_REQUIRES_NEW
        }

        // TODO use it for QUERY - probably 2x different SpringTxContextProvider
        private val txDefNever = object : TransactionDefinition {
            override fun getPropagationBehavior(): Int = PROPAGATION_NEVER
            override fun isReadOnly(): Boolean = true
        }
    }

    @Autowired lateinit var txManagerDelegate: PlatformTransactionManager

    override fun retId(): String {
        return txManagerName
    }

    private fun retTxManager(): AbstractPlatformTransactionManager {
        InfrastructureConfigurationAssert.assertTrue(
            txManagerDelegate is AbstractPlatformTransactionManager,
            "Unsupported type of TransactionManager [expected=${AbstractPlatformTransactionManager::class.qualifiedName}, found=${txManagerDelegate::class.qualifiedName}]"
        )
        return txManagerDelegate as AbstractPlatformTransactionManager
    }

    override fun getTransaction(): SpringTxStatusAdapter {
        val txStatus = retTxManager().getTransaction(txDefReqNew)
        log.info("[$txStatus] get transaction")
        return SpringTxStatusAdapter(txStatus)
    }

    override fun commit(txStatus: ITransactionStatus) {
        log.info("[$txStatus] before commit")
        txStatus.toSpringTxStatus().txStatusDelegate.flush()
        retTxManager().commit(txStatus.toSpringTxStatus().txStatusDelegate)
        log.info("[$txStatus] after commit")
    }

    override fun rollback(txStatus: ITransactionStatus) {
        log.info("[$txStatus] before rollback")
        retTxManager().rollback(txStatus.toSpringTxStatus().txStatusDelegate)
        log.info("[$txStatus] after rollback")

    }

    // used for ctx to thread rebinding (coroutines impl)
    override fun suspend(txStatus: ITransactionStatus) {
        throw InfrastructureCoreException("Suspend function unsupported", PlatformExceptionCode.INF_CORE_UNSUPPORTED).bind("platformComponent", sig())
    }

    // used for ctx to thread rebinding (coroutines impl)
    override fun resume(txStatus: ITransactionStatus) {
        throw InfrastructureCoreException("Resume function unsupported", PlatformExceptionCode.INF_CORE_UNSUPPORTED).bind("platformComponent", sig())
    }

}

/* Each spring TransactionStatus has delegate transaction object type in it (like DefaultTransactionStatus / JDBC and so on).
*  Each specific tech transaction manager (which created it) consumes it in doSuspend() / doResume() from parent spring AbstractPlatformTransactionManager
*  AbstractPlatformTransactionManager need to be somehow tampered to access doSuspend() / doResume() methods which should do proper thread -> txState re-binding (not verified)*/
data class SpringTxStatusAdapter(val txStatusDelegate: TransactionStatus) : ITransactionStatus

fun ITransactionStatus.toSpringTxStatus(): SpringTxStatusAdapter {
    if (this is SpringTxStatusAdapter) {
        return this
    } else throw InfrastructureConfigurationException("Unexpected TransactionStatus implementation", PlatformExceptionCode.INF_CONF_DI)
        .bind("actual", this::class.qualifiedName!!)
        .bind("expected", SpringTxStatusAdapter::class.qualifiedName!!)
}