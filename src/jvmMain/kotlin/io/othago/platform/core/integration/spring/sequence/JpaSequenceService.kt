/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.integration.spring.sequence

import com.benasher44.uuid.Uuid
import io.othago.platform.core.sequence.ISequence
import io.othago.platform.core.sequence.ISequenceService
import org.springframework.stereotype.Service

@Service
class JpaSequenceService(var sequenceRepository: SequenceRepository) : ISequenceService {

    override fun getByType(type: String): JpaSequence = JpaSequence(sequenceRepository, type)
    override fun uuid(): Uuid = Uuid.randomUUID()

    //synchronization wrapped inner sequence object and should not leak (adding control complexity)
    override fun next(type: String): Long = getByType(type).next()

}

class JpaSequence(
    val repository: SequenceRepository,
    val type: String
) : ISequence {

    override fun retType(): String = type
    override fun next(): Long {
        synchronized(this) {
            // TODO we can fetch and update in single call natively (for performance)
            val initialSequence = repository.findByType(type).orElseGet { SequenceEntity(id = Uuid.randomUUID(), type = type) }
            val value = initialSequence.value
            initialSequence.value++
            repository.saveAndFlush(initialSequence)
            return value
        }
    }

    // TODO think of cloning bigger structures how to have less calls
//    val batchCounter: AtomicLong = AtomicLong(0)
//    fun nextBatch(type: String, batchSize: Int): Pair<Long, Int> {
//        synchronized(this) {
//            val initialSequence = sequenceRepository.findByNameOne(type)
//            val value = initialSequence.value + 1
//            initialSequence.value += batchSize
//            sequenceRepository.saveAndFlush(initialSequence)
//            return value to batchSize
//        }
//    }
}

//abstract class AbstractSequence<T, S>() : ISequenceAdmin<T, S> {
//    val elemType: Class<T>
//    var sequenceId: String? = null
//        protected set
//    protected var masterLock = Any()
//    protected var mode: SequenceMode = SequenceMode.MAINTENANCE
//
//    constructor(sequenceId: String?) : this() {
//        this.sequenceId = sequenceId
//    }
//
//    fun setMode(mode: SequenceMode) {
//        synchronized(masterLock) { this.mode = mode }
//    }
//
//    fun getMode(): SequenceMode {
//        synchronized(masterLock) { return mode }
//    }
//
//    @get:Throws(PlatformCoreException::class) val next: T
//        get() {
//            synchronized(masterLock) {
//                return if (mode.equals(SequenceMode.ACTIVE)) {
//                    next()
//                } else {
//                    throw PlatformCoreProcessingException("Attempt to poll next element from not Active sequence")
//                }
//            }
//        }
//
//    protected abstract operator fun next(): T
//    @Throws(PlatformCoreException::class)
//    fun getNextBatch(size: Long): List<T> {
//        synchronized(masterLock) {
//            return if (mode.equals(SequenceMode.ACTIVE)) {
//                nextBatch(size)
//            } else {
//                throw PlatformCoreProcessingException("Attempt to poll next batch of elements from not Active sequence")
//            }
//        }
//    }
//
//    protected abstract fun nextBatch(size: Long): List<T>
//
//    init {
//        elemType = PlatformReflectHelper.calculateTargetGenericClass(this)
//    }
//}
