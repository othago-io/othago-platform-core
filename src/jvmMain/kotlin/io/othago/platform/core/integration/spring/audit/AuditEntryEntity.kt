/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@file:Suppress("JpaAttributeTypeInspection")

package io.othago.platform.core.integration.spring.audit

import com.benasher44.uuid.Uuid
import io.othago.platform.core.entity.ICreatedEntity
import io.othago.platform.core.entity.IIdentityEntity
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonArray
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

/*

CREATE TABLE audit_entries
(
    id              UUID DEFAULT uuid_generate_v4(),
    created_at      TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    created_by_id   UUID NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',

    transaction_id  UUID,

    action_type     TEXT,
    action_id       UUID,
    action_details  JSON NOT NULL DEFAULT '{}',

    object_type     TEXT,
    object_id       UUID,
    object_details  JSON NOT NULL DEFAULT '{}',

    change_type     TEXT,
    change_details  JSON NOT NULL DEFAULT '{}',

    PRIMARY KEY (id)
);

 */

@Entity
@Table(name = "audit_entries")
data class AuditEntryEntity(
    /* TODO align table model */

    @Id override var id: Uuid,
    @Column override var createdById: Uuid,
    @Column override var createdAt: Instant = Clock.System.now(),

    @Column var txId: String,

    @Column var rootActionName: String = "UNKNOWN_ROOT_ACTION",
    @Column var execActionName: String = "UNKNOWN_ACTION",

    @Column var dataItemProto: Int,
    @Column var dataItemType: String,
    @Column var dataItemId: Uuid,
    @Column var dataItemVersion: Long,

    @Column var changeType: Int,
    @Column var changeDetails: JsonArray = JsonArray(listOf())

) : IIdentityEntity, ICreatedEntity

@Serializable
data class AuditEntryCrumbEntity(
    val fieldName: String,
    val oldValue: String?,
    val newValue: String?
)
