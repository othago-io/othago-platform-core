/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.integration.spring.web

import io.othago.platform.common.lang.isNull
import io.othago.platform.common.logging.PlatformLoggerFactory
import io.othago.platform.core.api.exception.PlatformException
import io.othago.platform.core.integration.spring.web.ErrorResponseHelper.buildErrorResponse
import io.othago.platform.core.integration.spring.web.ErrorResponseHelper.handleError
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest

@ControllerAdvice
class SpringHttpExceptionHandler(val httpReqTranslator : IHttpRequestTranslator,
                                 val errorResponseMappers: List<IErrorResponseExceptionMapper>) {
    companion object {
        protected val log = PlatformLoggerFactory.getLogger(this::class.qualifiedName)
    }

    // TODO test if there are other spring exception envelops out there
    private fun unwrapException(ex: Throwable): Throwable {
        return if (ex is IllegalStateException && ex.message?.startsWith("Invocation failure") == true) ex.cause ?: ex else ex
    }

    @ExceptionHandler(value = [Throwable::class])
    protected fun handleConflict(ex: Throwable, req: WebRequest): ResponseEntity<*> {
        val referer = httpReqTranslator.getReferer(req)
        //ex translators

        return when (val exception: Throwable = unwrapException(ex)) {
            // test Platform more specific exceptions upfront (order sensitive) in case need other HTTP status / msg
            is PlatformException -> {
                log.error("Handling platform exception ${exception.javaClass.name}.")
                handleError(
                    referer,
                    buildErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), exception)
                ) //properly handled -> sink for all not required specific http code / msg
            }
            else -> { //default unhandled from stack beyond othago platform
                // attempt to map known exceptions
                // TODO - auth loop with FE client / gateway / proxy
                //1. Servlet plugin
                //2. Keycloak plugin
                //3. HttpProxyRequestException:INF -> RESt client call platform ex wrapper
                // when httpProxyReqEx is created on REST RPC call tries to unpack errorResult / ErrorItems from embedded call,
                // sometimes its not included in rest call response (in case of network or infra issues)
                // here just repack this error struct if exists, if not create default one
                //4. Feign specific exception translator
                var errorResponse = errorResponseMappers.map { it.mapException(referer, ex) }.firstOrNull()

                if (errorResponse.isNull()) {
                    log.error("Handling unknown exception ${exception.javaClass.name}.", exception)
                    errorResponse = SpringErrorResponseExceptionMapper.mapException(referer, ex)!!
                }

                return errorResponse!!
            }
        }
    }
}


