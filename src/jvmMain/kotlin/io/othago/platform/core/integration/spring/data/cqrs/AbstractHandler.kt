/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.integration.spring.data.cqrs

import io.othago.platform.core.api.cqrs.CommandEvent
import io.othago.platform.core.api.cqrs.IResultObject
import io.othago.platform.core.api.cqrs.QueryEvent
import io.othago.platform.core.api.cqrs.RepositoryEvent
import io.othago.platform.core.api.exception.BusinessDataRetrievalException
import io.othago.platform.core.cqrs.ICommandHandler
import io.othago.platform.core.cqrs.IQueryHandler
import io.othago.platform.core.cqrs.IRepositoryEventHandler
import io.othago.platform.core.deploy.PlatformContext
import io.othago.platform.core.integration.spring.data.executeWithSpringData
import java.util.*
import kotlin.reflect.KProperty1

abstract class AbstractSpringDataCH<out CE : CommandEvent> : ICommandHandler<PlatformContext, CE> {
    final override fun handleEvent(): PlatformContext.(@UnsafeVariance CE) -> Unit {
        return { e ->
            executeWithSpringData {
                handleEventWithSpring()(e)
            }
        }
    }

    abstract fun handleEventWithSpring(): PlatformContext.(@UnsafeVariance CE) -> Unit
}

abstract class AbstractSpringDataQH<out QE : QueryEvent, R : IResultObject> : IQueryHandler<PlatformContext, QE, R> {
    final override fun handleEvent(): PlatformContext.(@UnsafeVariance QE) -> R {
        return { e ->
            executeWithSpringData {
                handleEventWithSpring()(e)
            }
        }
    }

    abstract fun handleEventWithSpring(): PlatformContext.(@UnsafeVariance QE) -> R
}

abstract class AbstractSpringDataRH<out RE : RepositoryEvent> : IRepositoryEventHandler<PlatformContext, RE> {
    final override fun handleEvent(): PlatformContext.(@UnsafeVariance RE) -> Unit {
        return { e ->
            executeWithSpringData {
                handleEventWithSpring()(e)
            }
        }
    }

    abstract fun handleEventWithSpring(): PlatformContext.(@UnsafeVariance RE) -> Unit
}

@Suppress("UNCHECKED_CAST")
fun <T, R> readInstanceProperty(instance: T, propertyName: String): R {
    // handle recursive
    val path: Queue<String> = LinkedList(propertyName.split("."))
    var i: Any? = instance
    while (path.size > 0) {
        i = readInstancePropertyInternal(i, path.remove())
    }
    // force a invalid cast exception if incorrect type here
    return i as R
}

fun <T> readInstancePropertyInternal(instance: T, propertyName: String): Any? {
    try {
        val property = instance!!::class.members
            // don't cast here to <Any, R>, it would succeed silently
            .first { it.name == propertyName } as KProperty1<Any, *>
        return property.get(instance)
    } catch (e: NoSuchElementException) {
        throw BusinessDataRetrievalException("Can't find $propertyName in $instance (of ${instance!!::class.qualifiedName}).")
    }
}


