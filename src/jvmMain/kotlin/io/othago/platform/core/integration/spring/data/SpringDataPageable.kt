/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.integration.spring.data

import io.othago.platform.core.api.cqrs.*
import io.othago.platform.core.integration.spring.data.cqrs.readInstanceProperty
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.support.PageableExecutionUtils

/*** Paging ***/
fun PageRequest.toPageable(): Pageable {
    return if (this.orders.isEmpty()) {
        org.springframework.data.domain.PageRequest.of(this.page, this.size)
    } else {
        org.springframework.data.domain.PageRequest.of(
            this.page, this.size,
            Sort.by(this.orders.map {
                Sort.Order(Sort.Direction.fromString(it.direction.name), it.property)
            })
        )
    }
}

fun PageableQueryEvent.toPageable(): Pageable {
    return if (this.pageRequest != null)
        this.pageRequest!!.toPageable()
    else
        org.springframework.data.domain.PageRequest.of(0, 10) // TODO defaults from somewhere
}


fun <T : IResultObject> Page<T>.toPageResult(): PageResult<T> = PageResult(
    content = this.content,
    pageable = PageableResult(
        sort = SortResult(this.pageable.sort.isSorted, this.pageable.sort.isUnsorted, this.pageable.sort.isEmpty),
        offset = this.pageable.offset,
        pageNumber = this.pageable.pageNumber,
        pageSize = this.pageable.pageSize,
        paged = this.pageable.isPaged,
        unpaged = this.pageable.isUnpaged
    ),
    totalPages = this.totalPages,
    totalElements = this.totalElements.toInt(),
    last = this.isLast,
    size = this.size,
    number = this.number,
    sort = SortResult(this.sort.isSorted, this.sort.isUnsorted, this.sort.isEmpty),
    first = this.isFirst,
    numberOfElements = this.numberOfElements,
    empty = this.isEmpty
)

fun Pageable.toPageRequest() = PageRequest(
    pageNumber,
    pageSize,
    sort.map { OrderRequest(it.property, SortDirection.valueOf(it.direction.name)) }.toList()
)

fun <T> applySortingAndPaginationInMemory(result: List<T>, pageable: Pageable): Page<T> {
    // sorting
    var sorted: List<T> = result
    if (pageable.sort.isSorted) {
        // TODO sort me
        val s1 = pageable.sort.toList()[0] // TODO handle stream and sort by multiple
        sorted = if (s1.isAscending) {
            result.sortedBy { elem: T -> readInstanceProperty<T, Comparable<Any>>(elem, s1.property) }
        } else {
            result.sortedByDescending { elem: T -> readInstanceProperty<T, Comparable<Any>>(elem, s1.property) }
        }
    }

    // pagination
    var paginated: List<T> = sorted
    if (sorted.size > pageable.pageSize) {
        // TODO casting to int might blow up
        val s = if (sorted.size > (pageable.offset + pageable.pageSize)) pageable.offset.toInt() + pageable.pageSize else sorted.size
        paginated = sorted.subList(pageable.offset.toInt(), s)
    }

    return PageableExecutionUtils.getPage(paginated, pageable) { result.size.toLong() }
}