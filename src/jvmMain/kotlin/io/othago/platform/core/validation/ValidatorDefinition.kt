/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.validation

import io.othago.platform.core.api.exception.BusinessDataConstraintsViolationException
import io.othago.platform.core.api.validation.constraint.Constraint
import kotlin.reflect.KClass
import kotlin.reflect.KProperty1
import kotlin.reflect.full.memberProperties

// TODO jvm reflection dependency

inline fun <E> validate(obj: E, block: Validator<E>.(E) -> Unit): E {
    val validator = Validator(obj).apply { block(obj) }
    if (validator.constraintViolations.isNotEmpty()) {
        throw BusinessDataConstraintsViolationException().bindConstraintViolations(validator.constraintViolations)
    }
    return obj
}

open class ValidatorDefinition<E : Any>(private val clazz: KClass<E>) {
    companion object {
        // TODO move to factory
        fun <E : Any> annotated(clazz: KClass<E>): ValidatorDefinition<E> {
            println("Producing annotated validator for ${clazz}")
            val result = ValidatorDefinition<E>(clazz)
            clazz.memberProperties.forEach {
                println("processing property: $it ${it.annotations}")
                val defProperty = result.PropertyDefinition(it)
                it.annotations.forEach { annotation ->
                    val constraint = validationAnnotationRegistry[annotation.annotationClass]?.invoke(annotation)
                    println("Annotation: $annotation [${annotation.annotationClass}] mapped to $constraint")
                    defProperty.add(constraint)
                }
                result.propertyList.add(defProperty)
            }
            return result
        }
    }

    val propertyList = mutableListOf<PropertyDefinition<*>>()

    open inner class PropertyDefinition<T>(val property: KProperty1<E, T?>) {
        val constraintList = mutableListOf<Constraint>()

        fun add(constraint: Constraint?) {
            if (constraint != null) {
                constraintList.add(constraint)
            }
        }

        fun validate(validator: Validator<E>) {
            val vp: Validator<E>.Property<T?> = validator.validate(property)
            println("property: $property")
            constraintList.forEach {
                println("constraint: $it")
                vp.validate(it) { value ->
                    val validator = validationConstraintValidatorRegistry[it::class]
                    val result = validator?.invoke(it, value) ?: true
                    println("validating: $it [${it::class}] against $value with $result using $validator")
                    result
                }
            }
        }

        override fun toString(): String {
            return "{ Property: ${property.name}, Constraints: ${constraintList.joinToString(",") { it.toString() }} }"
        }
    }

    operator fun PropertyDefinition<*>.invoke() {
        propertyList.add(this)
    }

    fun validate(obj: E) {
        val validator = Validator(obj)
        println("validating $obj with $validator")
        propertyList.forEach { property -> property.validate(validator) }
        if (validator.constraintViolations.isNotEmpty()) {
            throw BusinessDataConstraintsViolationException().bindConstraintViolations(validator.constraintViolations)
        }
    }

    override fun toString(): String {
        return "{\n Validator for ${clazz.qualifiedName},\n Properties: ${propertyList.joinToString(",") { it.toString() }} \n}"
    }
}
