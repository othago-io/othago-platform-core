/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.validation

//suspend fun <E, T> OValidator<E>.Property<T?>.isCoValid(validator: suspend (T) -> Boolean): OValidator<E>.Property<T?> =
//    this.coValidate(ValidConstraint) { it == null || validator(it) }

// TODO recursive schemas
// inline fun <E, T> Validator<E>.Property<T?>.validate(block: Validator<T>.(T) -> Unit): Validator<E>.Property<T?> {
//    val value = this.property.get(this.obj)
//    if (value != null) {
//        this.addConstraintViolations(
//            Validator(value).apply { block(value) }.constraintViolations.map {
//                DefaultConstraintViolation(
//                    property = "${this.property.name}.${it.property}",
//                    value = it.value,
//                    constraint = it.constraint
//                )
//            }
//        )
//    }
//    return this
//}

//        interpolate(
//            MessageBundle(
//                baseName = baseName,
//                locale = locale,
//                fallbackBaseName = this.constraint.messageBundle,
//                fallbackLocale = Locale.getDefault()),
//            this.constraint.messageKey,
//            this.constraint.messageParams)

// message bundling somewhere else
//import kotlin.reflect.full.declaredMemberProperties
//
//val messageKey: String
//    get() = "${this.javaClass.name}.message"
//
//val messageParams: Map<String, *>
//    get() = this.javaClass.kotlin.declaredMemberProperties
//        .asSequence()
//        .filter {
//            Constraint::class.declaredMemberProperties
//                .none { p -> p.name == it.name }
//        }
//        .map { it.name to it.get(this) }
//        .toMap()

//     override val messageKey: String =
//        if (min != Int.MIN_VALUE && max != Int.MAX_VALUE) super.messageKey
//        else if (min != Int.MIN_VALUE) "${this.javaClass.name}.min.message"
//        else if (max != Int.MAX_VALUE) "${this.javaClass.name}.max.message"
//        else super.messageKey
//
//    override val messageParams: Map<String, *>
//        get() = if (min != Int.MIN_VALUE && max != Int.MAX_VALUE)
//            mapOf(this::min.name to this.min, this::max.name to this.max)
//        else if (min != Int.MIN_VALUE)
//            mapOf(this::min.name to this.min)
//        else if (max != Int.MAX_VALUE)
//            mapOf(this::max.name to this.max)
//        else
//            mapOf(this::min.name to this.min, this::max.name to this.max)



//        suspend fun coValidate(constraint: (T?) -> Constraint, isValid: suspend (T?) -> Boolean): Property<T> {
//            val value = this.property.get(obj)
//            if (!isValid(value)) {
//                this@Validator.constraintViolations += ConstraintViolation(
//                    constraintName = constraint(value).name,
//                    dataItemId = obj!!.objectIdentity(),
//                    dataItemType = obj!!.objectType(),
//                    propertyPath = this.property.name,
//                    invalidValue = value?.toString(),
//                    // TODO i18n support
//                    message = "unknown",
//                    messageBundle = "unknown",
//                    messageCode = "unknown",
//                    messageParams = mapOf()
//                )
//            }
//            return this
//        }
//
//        suspend fun coValidate(constraint: Constraint, isValid: suspend (T?) -> Boolean): Property<T> =
//            coValidate({ constraint }, isValid)

// DSL extension

// ----

//LessConstraint::class
//LessOrEqualConstraint::class
//GreaterConstraint::class

//fun <E, T : Comparable<T>> OValidator<E>.Property<T?>.isGreaterThanOrEqualTo(value: T): OValidator<E>.Property<T?> =
//    this.validate(GreaterOrEqualConstraint(value)) { it == null || it >= value }
//fun <E, T : Comparable<T>> OValidator<E>.Property<T?>.isBetween(start: T, end: T): OValidator<E>.Property<T?> =
//    this.validate(BetweenConstraint(start, end)) { it == null || it in start.rangeTo(end) }
//fun <E, T : Comparable<T>> OValidator<E>.Property<T?>.isNotBetween(start: T, end: T): OValidator<E>.Property<T?> =
//    this.validate(NotBetweenConstraint(start, end)) { it == null || it !in start.rangeTo(end) }
//fun <E> OValidator<E>.Property<String?>.isEmpty(): OValidator<E>.Property<String?> =
//    this.validate(EmptyConstraint) { it == null || it.isEmpty() }
//fun <E> OValidator<E>.Property<String?>.isNotEmpty(): OValidator<E>.Property<String?> =
//    this.validate(NotEmptyConstraint) { it == null || it.isNotEmpty() }
//fun <E> OValidator<E>.Property<String?>.isBlank(): OValidator<E>.Property<String?> =
//    this.validate(BlankConstraint) { it == null || it.isBlank() }
//fun <E> OValidator<E>.Property<String?>.isEqualToIgnoringCase(value: String): OValidator<E>.Property<String?> =
//    this.validate(EqualsConstraint(value)) { it == null || it.equals(other = value, ignoreCase = true) }
//fun <E> OValidator<E>.Property<String?>.isNotEqualToIgnoringCase(value: String): OValidator<E>.Property<String?> =
//    this.validate(NotEqualsConstraint(value)) { it == null || !it.equals(other = value, ignoreCase = true) }
//fun <E> OValidator<E>.Property<String?>.isInIgnoringCase(vararg values: String): OValidator<E>.Property<String?> =
//    this.validate(InConstraint(values.toSet())) { it == null || values.toSet().any { e -> it.equals(other = e, ignoreCase = true) } }
//fun <E> OValidator<E>.Property<String?>.isInIgnoringCase(values: Iterable<String>): OValidator<E>.Property<String?> =
//    this.validate(InConstraint(values)) { it == null || values.any { e -> it.equals(other = e, ignoreCase = true) } }
//fun <E> OValidator<E>.Property<String?>.isNotInIgnoringCase(vararg values: String): OValidator<E>.Property<String?> =
//    this.validate(NotInConstraint(values.toSet())) { it == null || values.toSet().none { e -> it.equals(other = e, ignoreCase = true) } }
//fun <E> OValidator<E>.Property<String?>.isNotInIgnoringCase(values: Iterable<String>): OValidator<E>.Property<String?> =
//    this.validate(NotInConstraint(values)) { it == null || values.none { e -> it.equals(other = e, ignoreCase = true) } }
//fun <E> OValidator<E>.Property<String?>.hasSize(min: Int = Int.MIN_VALUE, max: Int = Int.MAX_VALUE): OValidator<E>.Property<String?> =
//    this.validate(SizeConstraint(min, max)) { it == null || it.length in min.rangeTo(max) }
//fun <E> OValidator<E>.Property<String?>.contains(value: String): OValidator<E>.Property<String?> =
//    this.validate(ContainsConstraint(value)) { it == null || it.contains(value) }
//fun <E> OValidator<E>.Property<String?>.containsIgnoringCase(value: String): OValidator<E>.Property<String?> =
//    this.validate(ContainsIgnoreCaseConstraint(value)) { it == null || it.contains(other = value, ignoreCase = true) }
//fun <E> OValidator<E>.Property<String?>.containsAll(vararg values: String): OValidator<E>.Property<String?> =
//    this.validate(ContainsAllConstraint(values.toSet())) { it == null || values.toSet().all { e -> it.contains(e) } }
//fun <E> OValidator<E>.Property<String?>.containsAll(values: Iterable<String>): OValidator<E>.Property<String?> =
//    this.validate(ContainsAllConstraint(values)) { it == null || values.all { e -> it.contains(e) } }
//fun <E> OValidator<E>.Property<String?>.containsAllIgnoringCase(vararg values: String): OValidator<E>.Property<String?> =
//    this.validate(ContainsAllConstraint(values.toSet())) { it == null || values.toSet().all { e -> it.contains(other = e, ignoreCase = true) } }
//fun <E> OValidator<E>.Property<String?>.containsAllIgnoringCase(values: Iterable<String>): OValidator<E>.Property<String?> =
//    this.validate(ContainsAllConstraint(values)) { it == null || values.all { e -> it.contains(other = e, ignoreCase = true) } }
//fun <E> OValidator<E>.Property<String?>.containsAny(vararg values: String): OValidator<E>.Property<String?> =
//    this.validate(ContainsAnyConstraint(values.toSet())) { it == null || values.toSet().any { e -> it.contains(e) } }
//fun <E> OValidator<E>.Property<String?>.containsAny(values: Iterable<String>): OValidator<E>.Property<String?> =
//    this.validate(ContainsAnyConstraint(values)) { it == null || values.any { e -> it.contains(e) } }
//fun <E> OValidator<E>.Property<String?>.containsAnyIgnoringCase(vararg values: String): OValidator<E>.Property<String?> =
//    this.validate(ContainsAnyConstraint(values.toSet())) { it == null || values.toSet().any { e -> it.contains(other = e, ignoreCase = true) } }
//fun <E> OValidator<E>.Property<String?>.containsAnyIgnoringCase(values: Iterable<String>): OValidator<E>.Property<String?> =
//    this.validate(ContainsAnyConstraint(values)) { it == null || values.any { e -> it.contains(other = e, ignoreCase = true) } }
//fun <E> OValidator<E>.Property<String?>.doesNotContain(value: String): OValidator<E>.Property<String?> =
//    this.validate(NotContainConstraint(value)) { it == null || !it.contains(value) }
//fun <E> OValidator<E>.Property<String?>.doesNotContainIgnoringCase(value: String): OValidator<E>.Property<String?> =
//    this.validate(NotContainConstraint(value)) { it == null || !it.contains(other = value, ignoreCase = true) }
//fun <E> OValidator<E>.Property<String?>.doesNotContainAll(vararg values: String): OValidator<E>.Property<String?> =
//    this.validate(NotContainAllConstraint(values.toSet())) { it == null || !values.toSet().all { e -> it.contains(e) } }
//fun <E> OValidator<E>.Property<String?>.doesNotContainAll(values: Iterable<String>): OValidator<E>.Property<String?> =
//    this.validate(NotContainAllConstraint(values)) { it == null || !values.all { e -> it.contains(e) } }
//fun <E> OValidator<E>.Property<String?>.doesNotContainAllIgnoringCase(vararg values: String): OValidator<E>.Property<String?> =
//    this.validate(NotContainAllConstraint(values.toSet())) { it == null || !values.toSet().all { e -> it.contains(other = e, ignoreCase = true) } }
//fun <E> OValidator<E>.Property<String?>.doesNotContainAllIgnoringCase(values: Iterable<String>): OValidator<E>.Property<String?> =
//    this.validate(NotContainAllConstraint(values)) { it == null || !values.all { e -> it.contains(other = e, ignoreCase = true) } }
//fun <E> OValidator<E>.Property<String?>.doesNotContainAny(vararg values: String): OValidator<E>.Property<String?> =
//    this.validate(NotContainAnyConstraint(values.toSet())) { it == null || !values.toSet().any { e -> it.contains(e) } }
//fun <E> OValidator<E>.Property<String?>.doesNotContainAny(values: Iterable<String>): OValidator<E>.Property<String?> =
//    this.validate(NotContainAnyConstraint(values)) { it == null || !values.any { e -> it.contains(e) } }
//fun <E> OValidator<E>.Property<String?>.doesNotContainAnyIgnoringCase(vararg values: String): OValidator<E>.Property<String?> =
//    this.validate(NotContainAnyConstraint(values.toSet())) { it == null || !values.toSet().any { e -> it.contains(other = e, ignoreCase = true) } }
//fun <E> OValidator<E>.Property<String?>.doesNotContainAnyIgnoringCase(values: Iterable<String>): OValidator<E>.Property<String?> =
//    this.validate(NotContainAnyConstraint(values)) { it == null || !values.any { e -> it.contains(other = e, ignoreCase = true) } }
//fun <E> OValidator<E>.Property<String?>.matches(regex: Regex): OValidator<E>.Property<String?> =
//    this.validate(MatchesConstraint(regex)) { it == null || it.matches(regex) }
//fun <E> OValidator<E>.Property<String?>.doesNotMatch(regex: Regex): OValidator<E>.Property<String?> =
//    this.validate(NotMatchConstraint(regex)) { it == null || !it.matches(regex) }
//fun <E> OValidator<E>.Property<String?>.contains(regex: Regex): OValidator<E>.Property<String?> =
//    this.validate(ContainsRegexConstraint(regex)) { it == null || it.contains(regex) }
//fun <E> OValidator<E>.Property<String?>.doesNotContain(regex: Regex): OValidator<E>.Property<String?> =
//    this.validate(NotContainRegexConstraint(regex)) { it == null || !it.contains(regex) }
//fun <E> OValidator<E>.Property<String?>.startsWith(prefix: String): OValidator<E>.Property<String?> =
//    this.validate(StartsWithConstraint(prefix)) { it == null || it.startsWith(prefix) }
//fun <E> OValidator<E>.Property<String?>.startsWithIgnoringCase(prefix: String): OValidator<E>.Property<String?> =
//    this.validate(StartsWithConstraint(prefix)) { it == null || it.startsWith(prefix = prefix, ignoreCase = true) }
//fun <E> OValidator<E>.Property<String?>.doesNotStartWith(prefix: String): OValidator<E>.Property<String?> =
//    this.validate(NotStartWithConstraint(prefix)) { it == null || !it.startsWith(prefix) }
//fun <E> OValidator<E>.Property<String?>.doesNotStartWithIgnoringCase(prefix: String): OValidator<E>.Property<String?> =
//    this.validate(NotStartWithConstraint(prefix)) { it == null || !it.startsWith(prefix = prefix, ignoreCase = true) }
//fun <E> OValidator<E>.Property<String?>.endsWith(suffix: String): OValidator<E>.Property<String?> =
//    this.validate(EndsWithConstraint(suffix)) { it == null || it.endsWith(suffix) }
//fun <E> OValidator<E>.Property<String?>.endsWithIgnoringCase(suffix: String): OValidator<E>.Property<String?> =
//    this.validate(EndsWithConstraint(suffix)) { it == null || it.endsWith(suffix = suffix, ignoreCase = true) }
//fun <E> OValidator<E>.Property<String?>.doesNotEndWith(suffix: String): OValidator<E>.Property<String?> =
//    this.validate(NotEndWithConstraint(suffix)) { it == null || !it.endsWith(suffix) }
//fun <E> OValidator<E>.Property<String?>.doesNotEndWithIgnoringCase(suffix: String): OValidator<E>.Property<String?> =
//    this.validate(NotEndWithConstraint(suffix)) { it == null || !it.endsWith(suffix = suffix, ignoreCase = true) }
//fun <E> OValidator<E>.Property<String?>.isEmail(): OValidator<E>.Property<String?> =
//    this.validate(EmailConstraint) {
//        it == null || it.matches(Regex(
//            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"))
//    }
//fun <E> OValidator<E>.Property<String?>.isWebsite(): OValidator<E>.Property<String?> =
//    this.validate(WebsiteConstraint) {
//        it == null || it.matches(Regex(
//            "^(https?:\\/\\/)?([a-zA-Z0-9]+(-?[a-zA-Z0-9])*\\.)+[\\w]{2,}(\\/\\S*)?\$"))
//    }
//fun <E, T> OValidator<E>.Property<T?>.isNull(): OValidator<E>.Property<T?> =
//    this.validate(NullConstraint) { it == null }
//fun <E, T> OValidator<E>.Property<T?>.isNotNull(): OValidator<E>.Property<T?> =
//    this.validate(NotNullConstraint) { it != null }
//fun <E, T> OValidator<E>.Property<T?>.isEqualTo(value: T): OValidator<E>.Property<T?> =
//    this.validate(EqualsConstraint(value)) { it == null || it == value }
//fun <E, T> OValidator<E>.Property<T?>.isNotEqualTo(value: T): OValidator<E>.Property<T?> =
//    this.validate(NotEqualsConstraint(value)) { it == null || it != value }
//fun <E, T> OValidator<E>.Property<T?>.isIn(vararg values: T): OValidator<E>.Property<T?> =
//    this.validate(InConstraint(values.toSet())) { it == null || values.contains(it) }
//fun <E, T> OValidator<E>.Property<T?>.isIn(values: Iterable<T>): OValidator<E>.Property<T?> =
//    this.validate(InConstraint(values)) { it == null || values.contains(it) }
//fun <E, T> OValidator<E>.Property<T?>.isNotIn(vararg values: T): OValidator<E>.Property<T?> =
//    this.validate(NotInConstraint(values.toSet())) { it == null || !values.contains(it) }
//fun <E, T> OValidator<E>.Property<T?>.isNotIn(values: Iterable<T>): OValidator<E>.Property<T?> =
//    this.validate(NotInConstraint(values)) { it == null || !values.contains(it) }
//fun <E, T> OValidator<E>.Property<T?>.isValid(validator: (T) -> Boolean): OValidator<E>.Property<T?> =
//    this.validate(ValidConstraint) { it == null || validator(it) }

