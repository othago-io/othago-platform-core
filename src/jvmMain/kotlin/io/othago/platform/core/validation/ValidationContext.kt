/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.validation

import io.othago.platform.core.api.serializers.toUuid
import io.othago.platform.core.api.validation.annotation.*
import io.othago.platform.core.api.validation.constraint.*
import kotlin.reflect.KClass

typealias ConstraintClass = KClass<out Constraint>
typealias IsValid = (Constraint, Any?) -> Boolean
typealias AnnotationClass = KClass<out Annotation>
typealias AnnotationToConstraintMapper = (Annotation) -> Constraint

// TODO jvm reflection dependency

// TODO think of the name different then context

// TODO validation context like json context to be able to customize it per ctx
val validationConstraintValidatorRegistry = mutableMapOf<ConstraintClass, IsValid>(

    NullConstraint::class to { _, v -> v == null },
    NotNullConstraint::class to { _, v -> v != null },
    EqualsConstraint::class to { c, v -> v == null || (c is EqualsConstraint && v == c.value) },
    EqualsIgnoreCaseConstraint::class to { c, v -> v == null || (c is EqualsIgnoreCaseConstraint && v is String && v.equalsIgnoreCase(c.value)) },
    NotEqualsConstraint::class to { c, v -> v == null || (c is EqualsConstraint && v != c.value) },
    NotEqualsIgnoreCaseConstraint::class to { c, v -> v == null || (c is EqualsIgnoreCaseConstraint && v is String && !v.equalsIgnoreCase(c.value)) },
    NotBlankConstraint::class to { _, v -> v == null || v is String && v.isNotBlank() },
    SizeConstraint::class to { c, v -> v == null || (c is SizeConstraint && v is String && v.length in c.min.rangeTo(c.max)) },
    UuidConstraint::class to { _, v -> v == null || (v is String && v.matches("^[a-f0-9]{8}(-[a-f0-9]{4}){4}[a-f0-9]{8}$".toRegex())) },
    LessConstraint::class to { c, v -> v == null || (c is LessConstraint && ((v is Long && v < c.value as Long) || (v is Int && v < c.value as Int))) },
    LessOrEqualConstraint::class to { c, v -> v == null || (c is LessOrEqualConstraint && ((v is Long && v <= c.value as Long) || (v is Int && v <= c.value as Int))) },
    GreaterConstraint::class to { c, v -> v == null || (c is GreaterOrEqualConstraint && ((v is Long && v >= c.value as Long) || (v is Int && v > c.value as Int))) },
    GreaterOrEqualConstraint::class to { c, v -> v == null || (c is GreaterOrEqualConstraint && ((v is Long && v >= c.value as Long) || (v is Int && v >= c.value as Int))) },

    BetweenConstraint::class to { c, v ->
        v == null || (c is BetweenConstraint && (
                (v is Long && (c.start as Long).rangeTo(c.end as Long).contains(v))
                        || (v is Int && (c.start as Int).rangeTo(c.end as Int).contains(v))
                ))
    },
    NotBetweenConstraint::class to { c, v ->
        v == null || (c is BetweenConstraint && (
                (v is Long && !(c.start as Long).rangeTo(c.end as Long).contains(v))
                        || (v is Int && !(c.start as Int).rangeTo(c.end as Int).contains(v))
                ))
    },
    EmptyConstraint::class to { _, v -> v == null || (v is String && v.isEmpty()) },
    NotEmptyConstraint::class to { _, v -> v == null || (
            (v is String && v.isNotEmpty()) ||
                    (v is List<*> && v.isNotEmpty())) },
    BlankConstraint::class to { _, v -> v == null || (v is String && v.isBlank()) },
    InConstraint::class to { c, v -> v == null || (c is InConstraint && c.values.any(v::equals)) },
    InIgnoreCaseConstraint::class to { c, v -> v == null || (c is InIgnoreCaseConstraint && v is String && c.values.any(v::equalsIgnoreCase)) },
    NotInConstraint::class to { c, v -> v == null || (c is NotInConstraint && c.values.none(v::equals)) },
    NotInIgnoreCaseConstraint::class to { c, v ->
        v == null || (c is NotInIgnoreCaseConstraint &&
                (v is String && c.values.none(v::equalsIgnoreCase)))
    },
    ContainsConstraint::class to { c, v ->
        v == null || (c is ContainsConstraint && (
                (v is String && c.value is String && v.contains(c.value) ||
                        (v is List<*> && v.contains(c.value)))
                ))
    },
    ContainsIgnoreCaseConstraint::class to { c, v ->
        v == null || (c is ContainsConstraint && (
                (v is String && c.value is String && v.contains(c.value, true)
                        || (v is List<*> && c.value is String && (v as List<String>).any(c.value::equalsIgnoreCase)))
                ))
    },
    ContainsAllConstraint::class to { c, v ->
        v == null || (c is ContainsAllConstraint && (
                (v is List<*> && c.values.all { e -> v.contains(e) })))
    },
    ContainsIgnoreCaseAllConstraint::class to { c, v ->
        v == null || (c is ContainsIgnoreCaseAllConstraint && (
                (v is List<*> && c.values.all { e -> e is String && (v as List<String>).all(e::equalsIgnoreCase) })))
    },
    ContainsAnyConstraint::class to { c, v ->
        v == null || (c is ContainsAnyConstraint && (
                (v is List<*> && c.values.any { e -> v.contains(e) })))
    },
    ContainsIgnoreCaseAnyConstraint::class to { c, v ->
        v == null || (c is ContainsIgnoreCaseAnyConstraint && (
                (v is List<*> && c.values.any { e -> e is String && (v as List<String>).any(e::equalsIgnoreCase) })))
    },
    NotContainConstraint::class to { c, v ->
        v == null || (c is NotContainConstraint && (
                (v is List<*> && !v.contains(c.value))))
    },
    NotContainIgnoreCaseConstraint::class to { c, v ->
        v == null || (c is NotContainIgnoreCaseConstraint && (
                (v is List<*> && c.value is String && (v as List<String>).any(c.value::equalsIgnoreCase))))
    },
    NotContainAllConstraint::class to { c, v ->
        v == null || (c is NotContainAllConstraint && (
                (v is List<*> && !c.values.all { e -> v.contains(e) })))
    },
    NotContainIgnoreCaseAllConstraint::class to { c, v ->
        v == null || (c is ContainsIgnoreCaseAllConstraint && (
                (v is List<*> && !c.values.all { e -> e is String && (v as List<String>).all(e::equalsIgnoreCase) })))
    },
    NotContainAnyConstraint::class to { c, v ->
        v == null || (c is NotContainAnyConstraint && (
                (v is List<*> && !c.values.any { e -> v.contains(e) })))
    },
    NotContainIgnoreCaseAnyConstraint::class to { c, v ->
        v == null || (c is NotContainIgnoreCaseConstraint && (
                (v is List<*> && c.value is String && !(v as List<String>).any(c.value::equalsIgnoreCase))))
    },
    MatchesConstraint::class to { c, v -> v == null || (c is MatchesConstraint && v is String && v.matches(c.regex)) },
    NotMatchConstraint::class to { c, v -> v == null || (c is NotMatchConstraint && v is String && !v.matches(c.regex)) },
    ContainsRegexConstraint::class to { c, v -> v == null || (c is ContainsRegexConstraint && v is String && v.contains(c.regex)) },
    NotContainRegexConstraint::class to { c, v -> v == null || (c is ContainsRegexConstraint && v is String && !v.contains(c.regex)) },
    StartsWithConstraint::class to { c, v -> v == null || (c is StartsWithConstraint && v is String && v.startsWith(c.prefix)) },
    StartsWithIgnoreCaseConstraint::class to { c, v -> v == null || (c is StartsWithIgnoreCaseConstraint && v is String && v.startsWith(c.prefix, true)) },
    NotStartWithConstraint::class to { c, v -> v == null || (c is NotStartWithConstraint && v is String && !v.startsWith(c.prefix)) },
    NotStartWithIgnoreCaseConstraint::class to { c, v -> v == null || (c is NotStartWithIgnoreCaseConstraint && v is String && !v.startsWith(c.prefix, true)) },
    EndsWithConstraint::class to { c, v -> v == null || (c is EndsWithConstraint && v is String && v.endsWith(c.suffix)) },
    EndsWithIgnoreCaseConstraint::class to { c, v -> v == null || (c is EndsWithIgnoreCaseConstraint && v is String && v.endsWith(c.suffix, true)) },
    NotEndWithConstraint::class to { c, v -> v == null || (c is NotEndWithConstraint && v is String && !v.endsWith(c.suffix)) },
    NotEndWithIgnoreCaseConstraint::class to { c, v -> v == null || (c is EndsWithIgnoreCaseConstraint && v is String && !v.endsWith(c.suffix, true)) },
    EmailConstraint::class to { c, v -> v == null || (c is EmailConstraint && v is String && v.matches(c.regex)) },
    WebsiteConstraint::class to { c, v -> v == null || (c is WebsiteConstraint && v is String && v.matches(c.regex)) },
//    ::class to { c, v -> true }, //
)

fun String.equalsIgnoreCase(other: String): Boolean = this.equals(other, true)

fun <T> Comparable<T>.rangeTo(end: Comparable<T>): ClosedRange<*> {
    return when (this) {
        is Long -> this.rangeTo(end as Long)
        else -> throw IllegalArgumentException("Unable to recognize properly type.")
    }
}

val validationAnnotationRegistry = mutableMapOf<AnnotationClass, AnnotationToConstraintMapper>(
    Null::class to { NullConstraint },
    NotNull::class to { NotNullConstraint },
    Equals::class to { EqualsConstraint((it as Equals).value) },
    EqualsIgnoreCase::class to { EqualsIgnoreCaseConstraint((it as EqualsIgnoreCase).value) },
    NotEquals::class to { NotEqualsConstraint((it as NotEquals).value) },
    NotEqualsIgnoreCase::class to { NotEqualsIgnoreCaseConstraint((it as NotEqualsIgnoreCase).value) },
    NotBlank::class to { NotBlankConstraint },
    Size::class to { SizeConstraint((it as Size).min, it.max) },
    Uuid::class to { UuidConstraint },
    Less::class to { LessConstraint((it as Less).value) },
    LessOrEqual::class to { LessOrEqualConstraint((it as LessOrEqual).value) },
    Greater::class to { GreaterConstraint((it as Greater).value) },
    GreaterOrEqual::class to { GreaterOrEqualConstraint((it as GreaterOrEqual).value) },
    Between::class to { it as Between; BetweenConstraint(it.start, it.end) },
    NotBetween::class to { it as NotBetween; NotBetweenConstraint(it.start, it.end) },
    Empty::class to { EmptyConstraint },
    NotEmpty::class to { NotEmptyConstraint },
    Blank::class to { BlankConstraint },
    In::class to { it as In; InConstraint(it.values.toList()) },
    InIgnoreCase::class to { it as InIgnoreCase; InIgnoreCaseConstraint(it.values.toList()) },
    NotIn::class to { it as NotIn; NotInConstraint(it.values.toList()) },
    NotInIgnoreCase::class to { it as NotInIgnoreCase; NotInIgnoreCaseConstraint(it.values.toList()) },
    Contains::class to { it as Contains; ContainsConstraint(it.value) },
    ContainsLong::class to { it as ContainsLong; ContainsConstraint(it.value) },
    ContainsUuid::class to { it as ContainsUuid; ContainsConstraint(it.value.toUuid()) },
    ContainsIgnoreCase::class to { it as ContainsIgnoreCase; ContainsIgnoreCaseConstraint(it.value) },
    ContainsAll::class to { it as ContainsAll; ContainsAllConstraint(it.values.toList()) },
    ContainsAllUuid::class to { it as ContainsAllUuid; ContainsAllConstraint(it.values.toList().map { v -> v.toUuid() }) },
    ContainsIgnoreCaseAll::class to { it as ContainsIgnoreCaseAll; ContainsIgnoreCaseAllConstraint(it.values.toList()) },
    ContainsAny::class to { it as ContainsAny; ContainsAnyConstraint(it.values.toList()) },
    ContainsAnyUuid::class to { it as ContainsAnyUuid; ContainsAnyConstraint(it.values.toList()) },
    ContainsIgnoreCaseAny::class to { it as ContainsIgnoreCaseAny; ContainsIgnoreCaseAnyConstraint(it.values.toList()) },
    NotContain::class to { it as NotContain; NotContainConstraint(it.value) },
    NotContainUuid::class to { it as NotContainUuid; NotContainConstraint(it.value) },
    NotContainIgnoreCase::class to { it as NotContainIgnoreCase; NotContainIgnoreCaseConstraint(it.value) },
    NotContainAll::class to { it as NotContainAll; NotContainAllConstraint(it.values.toList()) },
    NotContainIgnoreCaseAll::class to { it as NotContainIgnoreCaseAll; NotContainIgnoreCaseAllConstraint(it.values.toList()) },
    NotContainAny::class to { it as NotContainAny; NotContainAnyConstraint(it.values.toList()) },
    NotContainIgnoreCaseAny::class to { it as NotContainAll; NotContainIgnoreCaseAnyConstraint(it.values.toList()) },
    Matches::class to { it as Matches; MatchesConstraint(it.pattern) },
    NotMatch::class to { it as NotMatch; NotMatchConstraint(it.pattern) },
    ContainsRegex::class to { it as ContainsRegex; ContainsRegexConstraint(it.pattern) },
    NotContainRegex::class to { it as NotContainRegex; NotContainRegexConstraint(it.pattern) },
    StartsWith::class to { it as StartsWith; StartsWithConstraint(it.prefix) },
    StartsWithIgnoreCase::class to { it as StartsWithIgnoreCase; StartsWithIgnoreCaseConstraint(it.prefix) },
    NotStartWith::class to { it as NotStartWith; NotStartWithConstraint(it.prefix) },
    NotStartWithIgnoreCase::class to { it as NotStartWithIgnoreCase; NotStartWithIgnoreCaseConstraint(it.prefix) },
    EndsWith::class to { it as EndsWith; EndsWithConstraint(it.suffix) },
    EndsWithIgnoreCase::class to { it as EndsWithIgnoreCase; EndsWithIgnoreCaseConstraint(it.suffix) },
    NotEndWith::class to { it as NotEndWith; NotEndWithConstraint(it.suffix) },
    NotEndWithIgnoreCase::class to { it as NotEndWithIgnoreCase; NotEndWithIgnoreCaseConstraint(it.suffix) },
    Email::class to { EmailConstraint },
    Website::class to { WebsiteConstraint },
)
