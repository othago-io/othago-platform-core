/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.cqrs

import io.othago.platform.common.exception.InfrastructureValidationAssert
import io.othago.platform.core.api.cqrs.RepositoryEvent
import io.othago.platform.core.security.DefaultSecurityContextProvider
import io.othago.platform.core.security.ISecurityContextProvider
import io.othago.platform.core.security.SecurityContext
import io.othago.platform.core.transaction.DefaultTransactionContextProvider
import io.othago.platform.core.transaction.ITransactionContextProvider
import io.othago.platform.core.transaction.TransactionContext
import kotlinx.collections.immutable.*

interface IProcessingContextProvider<CTX : ProcessingContext> {
    fun createProcessingContext(): CTX
    fun cloneProcessingContext(actionName: String, ctx: CTX): CTX
}

abstract class AbstractProcessingContextProvider<CTX : ProcessingContext>(
    protected val securityContextProvider: ISecurityContextProvider = DefaultSecurityContextProvider,
    protected val txContextProvider: ITransactionContextProvider = DefaultTransactionContextProvider
) : IProcessingContextProvider<CTX>

open class ProcessingContextProvider(
    securityContextProvider: ISecurityContextProvider = DefaultSecurityContextProvider,
    txContextProvider: ITransactionContextProvider = DefaultTransactionContextProvider
) : AbstractProcessingContextProvider<ProcessingContext>(securityContextProvider, txContextProvider) {

    override fun createProcessingContext(): ProcessingContext {
        return ProcessingContext(securityContextProvider.provideSecurityContext(), txContextProvider.provideTransactionContext(), StateContext("INIT"))
    }

    override fun cloneProcessingContext(actionName: String, ctx: ProcessingContext): ProcessingContext {
        return ProcessingContext(ctx.secCtx, ctx.txCtx, ctx.stateCtx.clone(actionName))
    }

}

/* can't place convenient clone function here -> ctx implementing interface clone func with generics
 makes IEventHandler extension lambda function less handy (ctx access from lambda body) -> moved to provider iface*/
open class ProcessingContext(val secCtx: SecurityContext, val txCtx: TransactionContext, val stateCtx: StateContext) {
    val state = stateCtx // DSL / infix shorthand

    fun put(key: String, value: Any) = stateCtx.set(key, value)
    inline fun <reified T : Any> get(key: String): T {
        return stateCtx[key]
    }

    fun result(re: RepositoryEvent) = stateCtx.result(re)
    fun result(reList: List<RepositoryEvent>) = stateCtx.result(reList)
    fun retResult(): ImmutableList<RepositoryEvent> = stateCtx.retResult()

    /*private fun assertContextSelfRef(){
        //TODO should the check be on types or classes (all supertypeclasses?) check same for handler
        val inheritHierarchy = this::class.allSupertypes.toMutableList()

        //TODO message, expected type and found type (ex binds?)
        InfrastructureValidationAssert.assertTrue(this::class == inheritHierarchy[inheritHierarchy.size-2].arguments[0].type!!.classifier,"abc")
    }*/

}

class StateContext(val actionName: String, val accuState: ImmutableList<StateEntry> = persistentListOf()) {

    @PublishedApi
    internal val currentState = mutableMapOf<String, Any>()

    private val result = mutableListOf<RepositoryEvent>()

    private fun combineState(): ImmutableList<StateEntry> {
        var newAccu = mutableListOf<StateEntry>()
        newAccu.addAll(accuState)
        newAccu.add(StateEntry(actionName, currentState.toImmutableMap(), retResult()))
        return newAccu.toImmutableList()
    }

    operator fun set(key: String, value: Any) {
        currentState[key] = value //mutates state
    }

    inline operator fun <reified T : Any> get(key: String): T {
        val res = currentState[key]
        InfrastructureValidationAssert.assertTrue(
            res is T,
            "Programmatic error, type of value inserted into StateContext [${res!!::class.simpleName}] is different than expected while performing get [${T::class.simpleName}]"
        )
        return res as T
    }

    fun contains(key: String) = currentState.contains(key)

    infix fun result(re: RepositoryEvent) {
        re.executedActionName = actionName
        result.add(re)
    }

    infix fun result(reList: List<RepositoryEvent>) {
        reList.forEach { result(it) }
    }

    fun retResult(): ImmutableList<RepositoryEvent> {
        return result.toImmutableList()
    }

    fun clone(actionName: String) = StateContext(actionName, combineState()) //result state is global visible to collect results

    data class StateEntry(val actionName: String, val state: Map<String, Any> = persistentMapOf(), val result: List<RepositoryEvent> = persistentListOf())
}
