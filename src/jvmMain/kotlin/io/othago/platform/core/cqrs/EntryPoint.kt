/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.cqrs

import io.othago.platform.common.exception.InfrastructureValidationAssert
import io.othago.platform.core.api.cqrs.*
import io.othago.platform.core.env.IPlatformComponent

abstract class AbstractEntryPoint<CTX : ProcessingContext>(
    val name: String,
    private val procCtxProvider: IProcessingContextProvider<CTX>,
    executionAspectDelegate: IExecutionAspect = InvokeAspect
) : IPlatformComponent {

    @PublishedApi
    internal val executionAspect: IExecutionAspect = ExceptionHandlingAspect(executionAspectDelegate)
    override fun retId(): String = name

    //TODO add api flag / param to control readonly / rw tx creation
    //shape api down to tx provider / adapter
    fun retProcCtx(): CTX = procCtxProvider.createProcessingContext()

}

open class CommandEntryPoint<CTX : ProcessingContext>(
    name: String,
    procCtxProvider: IProcessingContextProvider<CTX>,
    protected val commandEventDispatcher: CommandEventDispatcher<CTX>,
    executionAspect: IExecutionAspect = InvokeAspect
) : AbstractEntryPoint<CTX>(name, procCtxProvider, executionAspect) {

    open fun <CE : CommandEvent> executeCommand(commandEvent: CE): CommandResult<RepositoryEvent> {
        return executionAspect.execute(retProcCtx(), commandEvent, { ctx, e ->
            val res = commandEventDispatcher.dispatchEvent(ctx, e)
            InfrastructureValidationAssert.assertTrue(
                res is List<RepositoryEvent>,
                "Returned command result doesn't match required result type"
            )
            CommandResult(res)
        })
    }
}

/* procCtxProvider & queryEventDispatcher exposed to satisfy inline reification -> DI object widely accessed anyway */
open class QueryEntryPoint<CTX : ProcessingContext>(
    name: String,
    procCtxProvider: IProcessingContextProvider<CTX>,
    val queryEventDispatcher: QueryEventDispatcher<CTX>,
    executionAspect: IExecutionAspect = InvokeAspect
) : AbstractEntryPoint<CTX>(name, procCtxProvider, executionAspect) {

    inline fun <QE : QueryEvent, reified R : IResultObject> executeQuery(queryEvent: QE): QueryResult<R> {
        return executionAspect.execute(retProcCtx(), queryEvent, { ctx, e ->
            val res = queryEventDispatcher.dispatchEvent(ctx, e)
            InfrastructureValidationAssert.assertTrue(
                res is R,
                "Returned query result doesn't match required result type"
            )
            QueryResult(res as R)
        })
    }

}

open class DirectQueryEntryPoint<CTX : ProcessingContext>(
    name: String,
    procCtxProvider: IProcessingContextProvider<CTX>,
    private val directQueryDispatcher: DirectQueryEventDispatcher<CTX>,
    executionAspect: IExecutionAspect = InvokeAspect
) : AbstractEntryPoint<CTX>(name, procCtxProvider, executionAspect) {

    fun <QE : QueryEvent, R : IResultObject> executeQuery(
        queryEvent: QE,
        queryHandler: IQueryHandler<CTX, QE, R>
    ): QueryResult<R> {
        return executionAspect.execute(retProcCtx(), queryEvent, { ctx, e ->
            val res = directQueryDispatcher.dispatchEvent(ctx, e, queryHandler)
            QueryResult(res)
        })
    }
}



