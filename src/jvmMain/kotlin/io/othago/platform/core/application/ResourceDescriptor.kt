/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.application

import io.othago.platform.core.cqrs.ICommandHandler
import io.othago.platform.core.cqrs.IQueryHandler
import io.othago.platform.core.cqrs.IRepositoryEventHandler
import io.othago.platform.core.cqrs.ProcessingContext
import io.othago.platform.core.env.EnvProcessingContext
import io.othago.platform.core.env.ProcessingEnvironment

data class ResourceDescriptor<CTX : ProcessingContext>(
    val commandHandlers: List<ICommandHandler<CTX, *>> = listOf(),
    val repositoryEventHandlers: List<IRepositoryEventHandler<CTX, *>> = listOf(),
    val queryHandlers: List<IQueryHandler<CTX, *, *>> = listOf(),
)

open class AppResBootStep<CTX : EnvProcessingContext<CTX>, ENV : ProcessingEnvironment<CTX>>(
    val appResDescriptorProvider: () -> ResourceDescriptor<CTX>
) : IBootStep<CTX, ENV> {

    override fun load(appInstance: ApplicationInstance, env: ENV) {
        env.registerResources(appResDescriptorProvider.invoke())
    }
}
