/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.application

import io.othago.platform.common.IPlatformResource
import io.othago.platform.common.ResourceRegistry
import io.othago.platform.common.exception.InfrastructureConfigurationAssert
import io.othago.platform.common.logging.PlatformLoggerFactory
import io.othago.platform.common.version.Version
import io.othago.platform.core.api.cqrs.CommandEvent
import io.othago.platform.core.api.cqrs.IResultObject
import io.othago.platform.core.api.cqrs.QueryEvent
import io.othago.platform.core.api.cqrs.RepositoryEvent
import io.othago.platform.core.cqrs.ICommandHandler
import io.othago.platform.core.cqrs.IQueryHandler
import io.othago.platform.core.cqrs.IRepositoryEventHandler
import io.othago.platform.core.cqrs.ProcessingContext
import io.othago.platform.core.env.EnvProcessingContext
import io.othago.platform.core.env.IPlatformComponent
import io.othago.platform.core.env.ProcessingEnvironment
import kotlinx.collections.immutable.toImmutableList

enum class FeatureState { INIT, LOADED, ACTIVE }
enum class FtrDependencyType { OPTIONAL, REQUIRED }

data class FeatureDependency(val info: FeatureInfo, val type: FtrDependencyType)

abstract class AbstractFtrBase(val parentFtr: FeatureInfo) {
    fun retDependencies(): List<FeatureDependency> = emptyList()
}

abstract class AbstractFtrEventHandler<CTX : ProcessingContext>(parentFtr: FeatureInfo) : AbstractFtrBase(parentFtr)

abstract class FtrCommandHandler<CTX : ProcessingContext, out CE : CommandEvent>(parentFtr: FeatureInfo) : ICommandHandler<CTX, CE>, AbstractFtrEventHandler<CTX>(parentFtr)
abstract class FtrQueryHandler<CTX : ProcessingContext, out QE : QueryEvent, out R : IResultObject>(parentFtr: FeatureInfo) : IQueryHandler<CTX, QE, R>,
    AbstractFtrEventHandler<CTX>(parentFtr)

abstract class FtrRepositoryEventHandler<CTX : ProcessingContext, out RE : RepositoryEvent>(parentFtr: FeatureInfo) : IRepositoryEventHandler<CTX, RE>,
    AbstractFtrEventHandler<CTX>(parentFtr)

data class FeatureDefinition<CTX : ProcessingContext>(
    val info: FeatureInfo,
    val ftrResProvider: () -> FtrResourceDescriptor<CTX>
)

data class FtrResourceDescriptor<CTX : ProcessingContext>(
    val commandHandlers: List<FtrCommandHandler<CTX, *>> = listOf(),
    val repositoryEventHandlers: List<FtrRepositoryEventHandler<CTX, *>> = listOf(),
    val queryHandlers: List<FtrQueryHandler<CTX, *, *>> = listOf(),
) {
    fun toResDescriptor() = ResourceDescriptor(commandHandlers, repositoryEventHandlers, queryHandlers)
    fun retFtrResources(): List<AbstractFtrBase> {
        val result = mutableListOf<AbstractFtrBase>()
        result.addAll(commandHandlers.toImmutableList())
        result.addAll(repositoryEventHandlers.toImmutableList())
        result.addAll(queryHandlers.toImmutableList())
        return result
    }
}

data class FeatureInfo(val id: String, val ver: Version) : Comparable<FeatureInfo> {
    override fun compareTo(other: FeatureInfo): Int {
        return if (this.id == other.id) {
            this.ver.compareTo(other.ver)
        } else {
            this.id.compareTo(other.id)
        }
    }

    fun matches(other: FeatureInfo) = (this.id == other.id && this.ver <= other.ver)

    fun sig() = "${this::class.simpleName}:${id}:${ver.sig()}"
}

data class Feature<CTX : ProcessingContext>(
    val info: FeatureInfo,
    val resources: FtrResourceDescriptor<CTX>,
    var state: FeatureState = FeatureState.INIT
) : IPlatformResource<FeatureInfo> {
    override fun retId(): FeatureInfo = info
    fun retFeatureDependencies(): List<FeatureDependency> = resources.retFtrResources().map { r -> r.retDependencies() }.flatten()

}

open class AppFtrBootStep<CTX : EnvProcessingContext<CTX>, ENV : ProcessingEnvironment<CTX>>(
    val ftrDef: FeatureDefinition<CTX>
) : IBootStep<CTX, ENV> {

    companion object {
        protected val log = PlatformLoggerFactory.getLogger(this::class.qualifiedName)
    }

    override fun load(appInstance: ApplicationInstance, env: ENV) {
        val ftr = Feature(ftrDef.info, ftrDef.ftrResProvider.invoke())
        env.featureManager.registerResource(ftr)
        env.registerResources(ftr.resources.toResDescriptor())
        ftr.state = FeatureState.LOADED
        log.info("[Feature loaded] ${ftr.info.sig()}")
    }

    override fun activate(appInstance: ApplicationInstance, env: ENV) {
        //validate dependencies
        env.featureManager.validateFeatureDependencies(ftrDef.info)
        val ftr = env.featureManager.retResource(ftrDef.info)
        ftr.state = FeatureState.ACTIVE
        log.info("[Feature Activated] ${ftr.info.sig()}")
    }
}

class FeatureManager<CTX : ProcessingContext>(name: String) : IPlatformComponent, ResourceRegistry<FeatureInfo, Feature<CTX>>(name) {

    override fun retId(): String = registryName

    fun highestFeatureMatch(ftrInfo: FeatureInfo): Feature<CTX>? {
        return retResourceList().filter { ftr -> ftr.info.id == ftrInfo.id }
            .reversed()
            .firstOrNull { ftr -> ftrInfo.matches(ftr.info) }
    }

    fun validateFeatureDependencies(ftrInfo: FeatureInfo) {
        retResource(ftrInfo).retFeatureDependencies().forEach { ftrDep ->
            if (ftrDep.type == FtrDependencyType.REQUIRED) {
                InfrastructureConfigurationAssert.assertNotNull(
                    highestFeatureMatch(ftrDep.info),
                    "${ftrInfo.sig()}[Unsatisfied Feature dependency] ${ftrDep.info.sig()}"
                )
            }
        }
    }
}