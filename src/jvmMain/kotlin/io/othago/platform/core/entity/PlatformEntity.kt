/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.entity

import com.benasher44.uuid.Uuid
import kotlinx.datetime.Instant
import kotlinx.serialization.json.JsonObject

/*

BaseEntity
    id             UUID DEFAULT uuid_generate_v4mc(),
    version        BIGINT DEFAULT 0,
WithCreatedEntity
    created_at     TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    created_by_id  UUID NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
WithModifiedEntity
    modified_at    TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    modified_by_id UUID NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',

*/
/* TODO move to data-api along with OL ex and assertions?
 *   audit entity depends on this, moving audit model / service would
 *   require to be moved on data-api generic models / structures*/
/* TODO JVM dependency in core */
interface IIdentityEntity {
    var id: Uuid
}

interface IVersionedEntity {
    val version: Long
}

interface ICreatedEntity {
    var createdAt: Instant
    var createdById: Uuid
}

interface IModifiedEntity {
    var modifiedAt: Instant
    var modifiedById: Uuid
}

interface ISerialEntity {
    var serial: Long
    var serialType: String
}

interface IPlatformEntity : IIdentityEntity, IVersionedEntity, ICreatedEntity, IModifiedEntity, ISerialEntity

interface INameEntity {
    var name: String
}

interface IPropertiesEntity {
    var properties: JsonObject
}

interface IDefinitionEntity {
    var definition: JsonObject
}
