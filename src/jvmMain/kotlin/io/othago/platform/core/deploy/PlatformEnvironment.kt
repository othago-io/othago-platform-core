/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.deploy

import io.othago.platform.core.application.ApplicationInstance
import io.othago.platform.core.cqrs.IProcessingContextProvider
import io.othago.platform.core.cqrs.ProcessingContext
import io.othago.platform.core.cqrs.StateContext
import io.othago.platform.core.env.*
import io.othago.platform.core.security.DefaultSecurityContextProvider
import io.othago.platform.core.security.ISecurityContextProvider
import io.othago.platform.core.security.SecurityContext
import io.othago.platform.core.transaction.DefaultTransactionContextProvider
import io.othago.platform.core.transaction.ITransactionContextProvider
import io.othago.platform.core.transaction.TransactionContext

/* Simple Scenario - no direct Environment ref within ProcessingContext */
class BasicEnvironment(
    procCtxProvider: IProcessingContextProvider<ProcessingContext>,
    procEnvCompProvider: ProcEnvComponentProvider<ProcessingContext> = ProcEnvComponentProvider()
) : ProcessingEnvironment<ProcessingContext>(procCtxProvider, procEnvCompProvider)

/* Default platform instances */
class PlatformContext(
    env: PlatformEnvironment,
    secCtx: SecurityContext,
    txCtx: TransactionContext,
    stateCtx: StateContext
) : EnvProcessingContext<PlatformContext>(env, secCtx, txCtx, stateCtx)

class PlatformContextProvider(
    securityContextProvider: ISecurityContextProvider = DefaultSecurityContextProvider,
    txContextProvider: ITransactionContextProvider = DefaultTransactionContextProvider
) : AbstractEnvProcessingContextProvider<PlatformContext, PlatformEnvironment>(securityContextProvider, txContextProvider,
    { procCtx, secCtx, txCtx, stateCtx -> PlatformContext(procCtx, secCtx, txCtx, stateCtx) })

class PlatformEnvironment(procCtxProvider: IProcessingContextProvider<PlatformContext>, procEnvCompProvider: ProcEnvComponentProvider<PlatformContext>) :
    ProcessingEnvironment<PlatformContext>(procCtxProvider, procEnvCompProvider)

class PlatformEnvironmentBuilder(
    val procCtxProvider: AbstractEnvProcessingContextProvider<PlatformContext, PlatformEnvironment> = PlatformContextProvider(),
    val procEnvCompProvider: ProcEnvComponentProvider<PlatformContext> = ProcEnvComponentProvider()
) : IEnvironmentBuilder<PlatformContext, PlatformEnvironment> {
    override fun build(appInstance: ApplicationInstance): PlatformEnvironment {
        val env = PlatformEnvironment(procCtxProvider, procEnvCompProvider)
        procCtxProvider.injectProcEnv(env)
        return env
    }
}