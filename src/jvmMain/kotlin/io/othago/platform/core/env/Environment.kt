/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.env

import io.othago.platform.common.ResourceRegistry
import io.othago.platform.common.logging.PlatformLoggerFactory
import io.othago.platform.core.application.ApplicationInstance
import io.othago.platform.core.application.FeatureManager
import io.othago.platform.core.application.ResourceDescriptor
import io.othago.platform.core.cqrs.*

const val DEF_REP_H_REG = "DefaultRepositoryEventHandlerRegistry"
const val DEF_REP_DPR = "DefaultRepositoryEventDispatcher"
const val DEF_CMD_H_REG = "DefaultCommandHandlerRegistry"
const val DEF_CMD_DPR = "DefaultCommandDispatcher"
const val DEF_CMD_EP = "DefaultCommandEntryPoint"
const val DEF_QRY_H_REG = "DefaultQueryHandlerRegistry"
const val DEF_QRY_DPR = "DefaultQueryHandlerRegistry"
const val DEF_QRY_DIR_DPR = "DefaultDirectQueryDispatcher"
const val DEF_QRY_EP = "DefaultQueryEntryPoint"
const val DEF_QRY_DIR_EP = "DefaultDirectQueryEntryPoint"

open class ProcEnvComponentProvider<CTX : ProcessingContext>(
    val reHandlerRegistryFactory: () -> RepositoryEventHandlerRegistry<CTX> = { RepositoryEventHandlerRegistry(DEF_REP_H_REG) },
    val reDispatcherFactory: (IProcessingContextProvider<CTX>, RepositoryEventHandlerRegistry<CTX>) -> RepositoryEventDispatcher<CTX> = { pcp, rehr ->
        RepositoryEventDispatcher(
            DEF_REP_DPR,
            pcp,
            rehr
        )
    },
    val cmdHandlerRegistryFactory: () -> CommandHandlerRegistry<CTX> = { CommandHandlerRegistry(DEF_CMD_H_REG) },
    val cmdDispatcherFactory: (IProcessingContextProvider<CTX>, CommandHandlerRegistry<CTX>, RepositoryEventDispatcher<CTX>) -> CommandEventDispatcher<CTX> = { pcp, cmdhr, red ->
        CommandEventDispatcher(
            DEF_CMD_DPR,
            pcp,
            cmdhr,
            red
        )
    },
    val cmdEntryPointFactory: (IProcessingContextProvider<CTX>, CommandEventDispatcher<CTX>) -> CommandEntryPoint<CTX> = { pcp, cmdd -> CommandEntryPoint(DEF_CMD_EP, pcp, cmdd) },
    val qryHandlerRegistryFactory: () -> QueryHandlerRegistry<CTX> = { QueryHandlerRegistry(DEF_QRY_H_REG) },
    val qryDispatcherFactory: (QueryHandlerRegistry<CTX>) -> QueryEventDispatcher<CTX> = { qhr -> QueryEventDispatcher(DEF_QRY_DPR, qhr) },
    val qryDirectDispatcherFactory: () -> DirectQueryEventDispatcher<CTX> = { DirectQueryEventDispatcher(DEF_QRY_DIR_DPR) },
    val qryEntryPointFactory: (IProcessingContextProvider<CTX>, QueryEventDispatcher<CTX>) -> QueryEntryPoint<CTX> = { pcp, qed -> QueryEntryPoint(DEF_QRY_EP, pcp, qed) },
    val queryDirectEntryPointFactory: (IProcessingContextProvider<CTX>, DirectQueryEventDispatcher<CTX>) -> DirectQueryEntryPoint<CTX> = { pcp, qedd ->
        DirectQueryEntryPoint(
            DEF_QRY_DIR_EP,
            pcp,
            qedd
        )
    },
)

open class ProcessingEnvironment<CTX : ProcessingContext>(val procCtxProvider: IProcessingContextProvider<CTX>, procEnvCompProvider: ProcEnvComponentProvider<CTX>) {

    private val componentRegistry = ResourceRegistry<String, IPlatformComponent>("EnvComponentRegistry")

    fun registerComponent(component: IPlatformComponent) = componentRegistry.registerResource(component)
    fun retComponent(name: String): IPlatformComponent = componentRegistry.retResource(name)

    val repositoryEventHandlerRegistry = procEnvCompProvider.reHandlerRegistryFactory.invoke()
    val repositoryEventDispatcher = procEnvCompProvider.reDispatcherFactory.invoke(procCtxProvider, repositoryEventHandlerRegistry)
    val commandHandlerRegistry = procEnvCompProvider.cmdHandlerRegistryFactory.invoke()
    val commandDispatcher = procEnvCompProvider.cmdDispatcherFactory.invoke(procCtxProvider, commandHandlerRegistry, repositoryEventDispatcher)
    val commandEntryPoint = procEnvCompProvider.cmdEntryPointFactory.invoke(procCtxProvider, commandDispatcher)
    val queryHandlerRegistry = procEnvCompProvider.qryHandlerRegistryFactory.invoke()
    val queryDispatcher = procEnvCompProvider.qryDispatcherFactory.invoke(queryHandlerRegistry)
    val queryDirectDispatcher = procEnvCompProvider.qryDirectDispatcherFactory.invoke()
    val queryEntryPoint = procEnvCompProvider.qryEntryPointFactory.invoke(procCtxProvider, queryDispatcher)
    val queryDirectEntryPoint = procEnvCompProvider.queryDirectEntryPointFactory.invoke(procCtxProvider, queryDirectDispatcher)

    val envLifecycleManager = EnvLifecycleManager(componentRegistry)
    val featureManager = FeatureManager<CTX>("FeatureManager")

    init {
        registerComponent(repositoryEventDispatcher)
        registerComponent(commandDispatcher)
        registerComponent(queryDispatcher)
        registerComponent(queryDirectDispatcher)
        registerComponent(featureManager)
    }

    fun registerResources(resDesc: ResourceDescriptor<CTX>) {
        resDesc.commandHandlers.forEach { cmdHandler -> commandHandlerRegistry.registerResource(cmdHandler) }
        resDesc.repositoryEventHandlers.forEach { reHandler -> repositoryEventHandlerRegistry.registerResource(reHandler) }
        resDesc.queryHandlers.forEach { qryHandler -> queryHandlerRegistry.registerResource(qryHandler) }
    }

}

/* DI app env config, ensures procEnv is injected into provider */
interface IEnvironmentBuilder<CTX : EnvProcessingContext<CTX>, ENV : ProcessingEnvironment<CTX>> {
    fun build(appInstance: ApplicationInstance): ENV
}

open class EnvLifecycleManager(val componentRegistry: ResourceRegistry<String, IPlatformComponent>) {

    companion object {
        protected val log = PlatformLoggerFactory.getLogger(this::class.qualifiedName)
    }

    fun init() {
        log.info("[Environment Lifecycle : init ]")
        componentRegistry.retResourceList().forEach { it.init() }
    }

    fun start() {
        log.info("[Environment Lifecycle : start ]")
        componentRegistry.retResourceList().forEach { it.start() }
    }

    fun stop() {
        log.info("[Environment Lifecycle : stop ]")
        componentRegistry.retResourceList().forEach { it.stop() }
    }
}


