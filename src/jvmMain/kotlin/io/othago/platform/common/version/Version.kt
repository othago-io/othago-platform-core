/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.common.version

data class Version(val major: Long = 0, val minor: Long = 0, val patch: Long = 0) : Comparable<Version> {
    override fun compareTo(other: Version): Int {
        if (this.major > other.major) return 1
        if (this.major < other.major) return -1
        if (this.minor > other.minor) return 1
        if (this.minor < other.minor) return -1
        if (this.patch > other.patch) return 1
        if (this.patch < other.patch) return -1
        return 0
    }

    fun sig() = "${this::class.simpleName}<${major},${minor},${patch}>"
}