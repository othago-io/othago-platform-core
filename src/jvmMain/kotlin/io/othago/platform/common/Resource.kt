/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.common

import io.othago.platform.common.exception.InfrastructureValidationAssert
import io.othago.platform.common.exception.PlatformCommonAssert
import io.othago.platform.common.logging.PlatformLoggerFactory
import kotlinx.collections.immutable.toImmutableList
import kotlinx.collections.immutable.toImmutableMap

interface IPlatformResource<ID : Comparable<ID>> {
    fun retId(): ID
    fun sig(): String = "${retId()}:${this::class.simpleName}"
}

open class ResourceRegistry<ID : Comparable<ID>, R : io.othago.platform.common.IPlatformResource<ID>>(val registryName: String) {

    companion object {
        protected val log = PlatformLoggerFactory.getLogger(this::class.qualifiedName)
    }

    private val registry = mutableMapOf<ID, R>()

    fun registrySig(): String {
        return "${this::class.simpleName}:${registryName}"
    }

    fun registerResource(res: R) {
        PlatformCommonAssert.assertNotContains(registry, res.retId(), "${registrySig()} Resource already registered: ${res.sig()}")
        registry[res.retId()] = res
        io.othago.platform.common.ResourceRegistry.Companion.log.info("${registrySig()} [Resource registered] ${res.sig()}")
    }

    fun getResource(resId: ID): R? {
        return registry[resId]
    }

    fun retResource(resId: ID): R {
        val res = getResource(resId)
        InfrastructureValidationAssert.assertNotNull(res, "${registrySig()} No resource registered with id: $resId")
        return res!!
    }

    fun retResourceMap(): Map<ID, R> {
        return registry.toSortedMap().toImmutableMap()
    }

    fun retResourceList(): List<R> {
        return registry.toSortedMap().values.toImmutableList()
    }

}
