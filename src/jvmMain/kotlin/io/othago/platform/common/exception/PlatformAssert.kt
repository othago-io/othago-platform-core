/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.common.exception

import io.othago.platform.common.lang.isNotNull
import io.othago.platform.common.lang.isNull
import io.othago.platform.core.api.exception.*
import io.othago.platform.core.entity.IPlatformEntity
import io.othago.platform.core.entity.IVersionedEntity

open class PlatformAssert<out EX : PlatformException>(
    private val exBuilder: (String, PlatformExceptionCode) -> EX,
    private val defaultExceptionCode: PlatformExceptionCode
) {

    private fun createException(msg: String, code: PlatformExceptionCode = defaultExceptionCode): EX = exBuilder.invoke(msg, code)

    fun assertNull(obj: Any?, msg: String, code: PlatformExceptionCode = defaultExceptionCode) {
        if (obj.isNotNull()) {
            throw exBuilder.invoke(msg, code)
        }
    }

    fun assertNotNull(obj: Any?, msg: String, code: PlatformExceptionCode = defaultExceptionCode) {
        if (obj.isNull()) {
            throw exBuilder.invoke(msg, code)
        }
    }

    fun assertTrue(condition: Boolean, msg: String, code: PlatformExceptionCode = defaultExceptionCode) {
        if (!condition) {
            throw exBuilder.invoke(msg, code)
        }
    }

    fun assertFalse(condition: Boolean, msg: String, code: PlatformExceptionCode = defaultExceptionCode) {
        if (condition) {
            throw exBuilder.invoke(msg, code)
        }
    }

    fun assertEquals(objA: Any?, objB: Any?, msg: String, code: PlatformExceptionCode = defaultExceptionCode) {
        assertTrue(objA == objB, msg, code)
    }

    fun <K : Any?> assertContains(map: Map<K, *>, key: K, msg: String, code: PlatformExceptionCode = defaultExceptionCode) {
        if (!map.contains(key)) {
            throw exBuilder.invoke(msg, code)
        }
    }

    fun <K : Any?> assertNotContains(map: Map<K, *>, key: K, msg: String, code: PlatformExceptionCode = defaultExceptionCode) {
        if (map.contains(key)) {
            throw exBuilder.invoke(msg, code)
        }
    }

    fun assertBlank(obj: String?, msg: String, code: PlatformExceptionCode = defaultExceptionCode) {
        if (!obj.isNullOrBlank()) {
            throw exBuilder.invoke(msg, code)
        }
    }

    fun assertNotBlank(obj: String?, msg: String, code: PlatformExceptionCode = defaultExceptionCode) {
        if (obj.isNullOrBlank()) {
            throw exBuilder.invoke(msg, code)
        }
    }
}

object PlatformCommonAssert : PlatformAssert<PlatformCommonException>(
    { m, c -> PlatformCommonException(m, c) },
    PlatformExceptionCode.PLF_COMMON
)

object InfrastructureValidationAssert : PlatformAssert<InfrastructureValidationException>(
    { m, c -> InfrastructureValidationException(m, c) },
    PlatformExceptionCode.INF_VALID_GENERIC
) {
    fun assertVersionMsg(version: Long, entity: IVersionedEntity, msg: String, code: PlatformExceptionCode = PlatformExceptionCode.INF_DATA_OPTIMISTIC_LOCK) =
        if (entity.version != version) {
            throw InfrastructureDataOLException(msg, code)
        } else {
        }

    fun assertVersionDsc(version: Long, entity: IVersionedEntity, entityDesc: String, code: PlatformExceptionCode = PlatformExceptionCode.INF_DATA_OPTIMISTIC_LOCK) =
        assertVersionMsg(version, entity, "Miss match in version for entity $entityDesc.", code)

    fun assertVersion(version: Long, entity: IPlatformEntity, code: PlatformExceptionCode = PlatformExceptionCode.INF_DATA_OPTIMISTIC_LOCK) =
        assertVersionDsc(version, entity, Assert.entityDesc(entity), code)

    fun assertHasChangesMsg(equals: Boolean, msg: String, code: PlatformExceptionCode = PlatformExceptionCode.INF_DATA_NO_CHANGES) = if (equals) {
        throw InfrastructureDataNoChangesException(msg, code)
    } else {
    }

    fun assertHasChangesDsc(equals: Boolean, entityDesc: String, code: PlatformExceptionCode = PlatformExceptionCode.INF_DATA_NO_CHANGES) =
        assertHasChangesMsg(equals, "No changes recorded for entity $entityDesc.", code)

    fun assertHasChanges(equals: Boolean, entity: IPlatformEntity, code: PlatformExceptionCode = PlatformExceptionCode.INF_DATA_NO_CHANGES) =
        assertHasChangesDsc(equals, Assert.entityDesc(entity), code)
}

object InfrastructureConfigurationAssert : PlatformAssert<InfrastructureConfigurationException>(
    { m, c -> InfrastructureConfigurationException(m, c) },
    PlatformExceptionCode.INF_CONF_GENERIC
)

object BusinessValidationAssert : PlatformAssert<BusinessValidationException>(
    { m, c -> BusinessValidationException(m, c) },
    PlatformExceptionCode.BUS_VALID_GENERIC
)

/* additional param required, dedicated assert to enforce compiler to guard api / function signatures*/
object BusinessFieldAssert {
    private val defaultExceptionCode = PlatformExceptionCode.BUS_VALID_FORM_FIELD
    fun assertNull(obj: Any?, fieldId: String, msg: String, code: PlatformExceptionCode = defaultExceptionCode) {
        if (obj.isNotNull()) {
            throw BusinessValidationFieldException(fieldId, msg, code)
        }
    }

    fun assertNotNull(obj: Any?, fieldId: String, msg: String, code: PlatformExceptionCode = defaultExceptionCode) {
        if (obj.isNull()) {
            throw BusinessValidationFieldException(fieldId, msg, code)
        }
    }

    fun assertTrue(condition: Boolean, fieldId: String, msg: String, code: PlatformExceptionCode = defaultExceptionCode) {
        if (!condition) {
            throw BusinessValidationFieldException(fieldId, msg, code)
        }
    }

    fun assertFalse(condition: Boolean, fieldId: String, msg: String, code: PlatformExceptionCode = defaultExceptionCode) {
        if (condition) {
            throw BusinessValidationFieldException(fieldId, msg, code)
        }
    }
}

// aliases - direct
val IVC = InfrastructureConfigurationAssert
val IVA = InfrastructureValidationAssert

// aliases - wrapped
object Assert {
    const val patternUUID = "^[a-f0-9]{8}(-[a-f0-9]{4}){4}[a-f0-9]{8}$"
    const val patternClassName = "[\\w_\\.]+"
    fun entityDesc(entity: Any, identifier: String, version: Long? = null) = "${entity.javaClass.canonicalName}#$identifier:${version ?: "unknown"}"
    fun entityDesc(entity: IVersionedEntity, identifier: String) = "${entity.javaClass.canonicalName}#$identifier:${entity.version}"
    fun entityDesc(entity: IPlatformEntity) = "${entity.javaClass.canonicalName}#${entity.id}:${entity.version}"
    val IC = InfrastructureConfigurationAssert
    val IV = InfrastructureValidationAssert
}
