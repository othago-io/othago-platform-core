/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.common.logging

import io.othago.platform.core.integration.logging.KotlinLoggerFactory


/* TODO kotlin common platform wrapper implementation,
when doing multi-platform exclude iface as expect here and move impl as "actual" to /integration/logging and wrap as platform dependent module */

/*expect*/ object PlatformLoggerFactory {

    fun getLogger(id: String?): IPlatformLogger = KotlinLoggerFactory.getLogger(id!!)

}