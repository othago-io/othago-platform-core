/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.examples

import io.othago.platform.core.api.cqrs.CommandEvent
import io.othago.platform.core.api.cqrs.RepositoryEvent
import io.othago.platform.core.application.ApplicationBuilder
import io.othago.platform.core.application.resDscr
import io.othago.platform.core.cqrs.ICommandHandler
import io.othago.platform.core.deploy.PlatformContext
import io.othago.platform.core.deploy.PlatformEnvironment
import io.othago.platform.core.env.IEnvironmentBuilder
import io.othago.platform.core.integration.spring.SpringTxEnvironmentBuilder
import io.othago.platform.core.integration.spring.data.AbstractSpringDataJpaTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.ComponentScan
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import testAppInstance
import javax.persistence.*

interface PersonRepository : CrudRepository<PersonEntity, Long>

@Entity
@Table(name = "PERSON")
class PersonEntity(@Id @GeneratedValue(strategy = GenerationType.AUTO) val id: Long?, var name: String)


class SpringDataJpaAppBuilder(procEnvBuilder: IEnvironmentBuilder<PlatformContext, PlatformEnvironment>) : ApplicationBuilder<PlatformContext, PlatformEnvironment>(procEnvBuilder)

/* Custom App Resources */
class CreatePersonCE(val name: String) : CommandEvent()
class UpdatePersonCE(val id: Long, val name: String) : CommandEvent()
class UserCreatedRE(val id: Long, val name: String) : RepositoryEvent()

@Component
class CreatePersonCEH : ICommandHandler<PlatformContext, CreatePersonCE> {
    @Autowired
    lateinit var personRepository: PersonRepository

    override fun handleEvent(): PlatformContext.(CreatePersonCE) -> Unit = { e ->
        val newUser = personRepository.save(PersonEntity(null, e.name))
        state result UserCreatedRE(newUser.id!!, newUser.name)
    }
}

@Component
class UpdatePersonCEH : ICommandHandler<PlatformContext, UpdatePersonCE> {
    @Autowired
    lateinit var personRepository: PersonRepository

    override fun handleEvent(): PlatformContext.(UpdatePersonCE) -> Unit = { e ->
        var existing = personRepository.findById(e.id).get()
        existing.name = e.name
        personRepository.save(existing)
        throw Throwable("Intent ex")
    }
}


@Transactional(propagation = Propagation.NEVER) //required to bootstrap jpaTxManager, transaction is manager via platform execution aspect
//@EnableTransactionManagement
@EnableJpaRepositories("io.othago.platform.examples")
@EntityScan("io.othago.platform.examples")
@ComponentScan(basePackages = ["io.othago.platform.examples"])
class SpringJpaAppExampleTest : AbstractSpringDataJpaTest() {

    @Autowired
    lateinit var appContext: ApplicationContext

    @Autowired
    lateinit var envBuilder: SpringTxEnvironmentBuilder

    @Autowired
    lateinit var personRepository: PersonRepository

    @Test
    fun bootstrapAppTransactionTest() {

        val springDataExampleAppBuilder = SpringDataJpaAppBuilder(envBuilder)
        //TODO common helpers
        val cmdHandlers = appContext.getBeansOfType(ICommandHandler::class.java).values.toList() as List<ICommandHandler<PlatformContext, *>>
        springDataExampleAppBuilder.registerApplicationResources { resDscr(cmdHandlers) }

        val env = springDataExampleAppBuilder.createApplication(testAppInstance())
        val resCreated = env.commandEntryPoint.executeCommand(CreatePersonCE("Joe"))

        val personCreatedList = personRepository.findAll().toList()

        assertEquals(1, personCreatedList.size)
        assertEquals("Joe", personCreatedList[0].name)

        try {
            env.commandEntryPoint.executeCommand(UpdatePersonCE((resCreated.payload[0] as UserCreatedRE).id, "Mike"))
        } catch (th: Throwable) {
        }

        val personUpdatedList = personRepository.findAll().toList()

        assertEquals(1, personUpdatedList.size)
        assertEquals("Joe", personUpdatedList[0].name) //mike should be rolled back
    }

}
