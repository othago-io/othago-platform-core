/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.integration.spring.data

import io.othago.platform.test.unitils.JdbcTestData
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.CrudRepository
import org.springframework.test.context.ActiveProfiles
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table


/* Test spring data and unitils setup covered by various scenarios: hibernate auto-ddl / flyway */

interface AddressRepository : CrudRepository<AddressEntity, Long>

@Entity
@Table(name = "ADDRESS")
class AddressEntity(@Id val id: Long, val venue: String)

class SpringDataJpaTest : AbstractSpringDataJpaTest() {

    @Autowired
    lateinit var addressRepository: AddressRepository

    @Test
    @JdbcTestData("/test/data/testDataSet.json")
    fun bootstrapAppTest() {
        val addressesList = addressRepository.findAll().toList().sortedBy { it.id }
        Assertions.assertEquals(2, addressesList.size)
        Assertions.assertEquals("Apple street", addressesList[0].venue)
        Assertions.assertEquals("Cherry square", addressesList[1].venue)
    }
}


@ActiveProfiles("test-flyway", inheritProfiles = false)
class SpringDataJpaFlywayTest : AbstractSpringDataJpaTest() {

    @Autowired
    lateinit var addressRepository: AddressRepository

    @Test
    fun bootstrapAppTest() {
        val addressesList = addressRepository.findAll().toList().sortedBy { it.id }
        Assertions.assertEquals(2, addressesList.size)
        Assertions.assertEquals("Apple street", addressesList[0].venue)
        Assertions.assertEquals("Cherry square", addressesList[1].venue)
    }

}