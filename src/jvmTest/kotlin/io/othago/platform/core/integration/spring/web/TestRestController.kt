/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.integration.spring.web

import io.othago.platform.core.api.cqrs.QueryEvent
import io.othago.platform.core.api.cqrs.QueryResult
import io.othago.platform.core.api.exception.PlatformException
import io.othago.platform.core.application.resDscr
import io.othago.platform.core.cqrs.IQueryHandler
import io.othago.platform.core.cqrs.StringResult
import io.othago.platform.core.deploy.PlatformApplicationBuilder
import io.othago.platform.core.deploy.PlatformContext
import io.othago.platform.core.deploy.PlatformEnvironment
import io.othago.platform.core.deploy.PlatformEnvironmentBuilder
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import testAppInstance

class TestRestQE(val content: String) : QueryEvent()

@Component
class TestRestQEH : IQueryHandler<PlatformContext, TestRestQE, StringResult> {
    override fun handleEvent(): PlatformContext.(TestRestQE) -> StringResult = { e ->
        throw PlatformException("abc")//e.content
    }
}

@RestController
class TestRestController {

    //todo create and move to spring config
    private val appBuilder = PlatformApplicationBuilder(PlatformEnvironmentBuilder())
    private lateinit var env: PlatformEnvironment

    init {
        appBuilder.registerApplicationResources { resDscr(qeHandlers = listOf(TestRestQEH())) }
        env = appBuilder.createApplication(testAppInstance())
    }

    @GetMapping("/api/v1/testRestQuery")
    fun testRestQuery(): QueryResult<StringResult> {
        return env.queryEntryPoint.executeQuery(TestRestQE("xyz"))
    }

}