/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.deploy

import io.othago.platform.core.api.cqrs.CommandEvent
import io.othago.platform.core.api.cqrs.RepositoryEvent
import io.othago.platform.core.cqrs.ICommandHandler
import io.othago.platform.core.cqrs.IRepositoryEventHandler
import io.othago.platform.core.env.EnvProcessingContext
import java.util.concurrent.atomic.AtomicBoolean

open class TestCE : CommandEvent()
open class TestReactRE : RepositoryEvent()

open class TestCommandCEH<CTX : EnvProcessingContext<CTX>, E : TestCE>(val re: RepositoryEvent) : ICommandHandler<CTX, E> {
    override fun handleEvent(): CTX.(E) -> Unit {
        return { state result re }
    }
}

open class TestReactREH<CTX : EnvProcessingContext<CTX>, E : TestReactRE> : IRepositoryEventHandler<CTX, E> {
    var execFlag = AtomicBoolean(false)

    override fun handleEvent(): CTX.(E) -> Unit {
        return { execFlag.set(true) }
    }
}

/* Platform resources */
object TestP1CE : TestCE()
object TestReactP1RE : TestReactRE()

object TestCommandP1CEH : TestCommandCEH<PlatformContext, TestP1CE>(TestReactP1RE)
object TestCommandP1ExceptionCEH : ICommandHandler<PlatformContext, TestCE> {
    override fun handleEvent(): PlatformContext.(TestCE) -> Unit {
        throw Throwable("Test Exception")
    }
}

object TestReactP1REH : TestReactREH<PlatformContext, TestReactP1RE>()


