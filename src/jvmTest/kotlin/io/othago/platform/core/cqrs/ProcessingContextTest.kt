/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.cqrs

import io.othago.platform.core.api.cqrs.RepositoryEvent
import io.othago.platform.core.api.cqrs.retActionName
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Test
import kotlin.reflect.full.allSupertypes

class ProcessingContextTest {

    private val processingContextProvider = ProcessingContextProvider()

    class DummyRepositoryEvent : RepositoryEvent()

    @Test
    fun processingContextClone() {
        //ROOT LVL
        val initCtx = processingContextProvider.createProcessingContext()
        initCtx.stateCtx["K0"] = "V0"

        assertEquals("INIT", initCtx.stateCtx.actionName)
        assertEquals(0, initCtx.stateCtx.accuState.size)
        assertEquals("V0", initCtx.stateCtx["K0"])

        //Depth Level 1
        var clonedCtxL1 = processingContextProvider.cloneProcessingContext("action_1", initCtx)
        clonedCtxL1.stateCtx["K1"] = "V1"

        assertEquals("action_1", clonedCtxL1.stateCtx.actionName)
        assertEquals(1, clonedCtxL1.stateCtx.accuState.size)
        assertFalse(clonedCtxL1.stateCtx.contains("K0"))
        assertEquals("V1", clonedCtxL1.stateCtx["K1"])

        //check init
        val init1 = clonedCtxL1.stateCtx.accuState.last()
        assertEquals("INIT", init1.actionName)
        assertEquals("V0", init1.state["K0"])
        assertFalse(init1.state.contains("K1"))

        //Depth Level 2
        var clonedCtxL2 = processingContextProvider.cloneProcessingContext("action_2", clonedCtxL1)
        clonedCtxL2.stateCtx["K2"] = "V2"

        assertEquals("action_2", clonedCtxL2.stateCtx.actionName)
        assertEquals(2, clonedCtxL2.stateCtx.accuState.size)
        assertFalse(clonedCtxL2.stateCtx.contains("K0"))
        assertFalse(clonedCtxL2.stateCtx.contains("K1"))
        assertEquals("V2", clonedCtxL2.stateCtx["K2"])

        //check init
        val init2 = clonedCtxL2.stateCtx.accuState.first()
        assertEquals("INIT", init2.actionName)
        assertEquals("V0", init2.state["K0"])
        assertFalse(init2.state.contains("K1"))
        assertFalse(init2.state.contains("K2"))

        //check accu L1
        val accuL1 = clonedCtxL2.stateCtx.accuState.last()
        assertEquals("action_1", accuL1.actionName)
        assertFalse(accuL1.state.contains("K0"))
        assertEquals("V1", accuL1.state["K1"])
        assertFalse(accuL1.state.contains("K2"))

    }

    @Test
    fun processingContextDslHelpers() {

        val initCtx = processingContextProvider.createProcessingContext()
        initCtx.stateCtx["K00"] = "V00"
        assertEquals("V00", initCtx.stateCtx["K00"])

        initCtx.stateCtx.set("K00", "V01")
        assertEquals("V01", initCtx.stateCtx.get("K00"))

        initCtx.state["K00"] = "V02"
        assertEquals("V02", initCtx.state["K00"])

        initCtx.state.set("K00", "V03")
        assertEquals("V03", initCtx.get("K00"))

        initCtx.state.result(DummyRepositoryEvent())
        assertEquals(1, initCtx.retResult().size)
        assertEquals("dummyRepositoryEvent", initCtx.retResult().first().retActionName())

        initCtx.state result DummyRepositoryEvent()
        assertEquals(2, initCtx.retResult().size)
        assertEquals("dummyRepositoryEvent", initCtx.retResult().last().retActionName())
    }

    @Test
    fun test() {
        val inheritHierachy = C::class.allSupertypes.toMutableList()
        println(inheritHierachy[inheritHierachy.size - 2].arguments[0].type!!.classifier)
    }

}

open class A<Z>

open class B : A<String>()

class C : B()

