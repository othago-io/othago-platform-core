/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.cqrs

import io.othago.platform.core.api.cqrs.*
import io.othago.platform.core.deploy.PlatformContext
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import procEnv
import java.io.Serializable

class HandlerTest {

    @Test
    fun handleEventTest() {
        val testEvent = TestEvent("Sample content")
        val env = procEnv()
        val procCtx = env.procCtxProvider.createProcessingContext()
        val res = TestEventHandler.handleEvent().invoke(procCtx, testEvent)

        assertEquals(res, "Handled [${testEvent.sig()}] ${testEvent.content}")
    }
}

class TestEvent(val content: String) : IPlatformEvent

object TestEventHandler : IEventHandler<PlatformContext, TestEvent, String> {

    override fun handleEvent(): PlatformContext.(TestEvent) -> String {
        return { e ->
            "Handled [${e.sig()}] ${e.content}"
        }
    }

}

class CommandHandlerTest {

    @Test
    fun handleEventTest() {
        val env = procEnv()
        val procCtx = env.procCtxProvider.createProcessingContext()
        val testEvent = CommandTestCE("Sample command")
        CommandTestEventHandler.handleEvent().invoke(procCtx, testEvent)

        assertEquals("Sample command result: [${testEvent.sig()}] (${testEvent.content})", (procCtx.retResult()[0] as CommandTestRE).content)
    }
}

class CommandTestCE(val content: String) : CommandEvent()
class CommandTestRE(val content: String) : RepositoryEvent()

object CommandTestEventHandler : ICommandHandler<PlatformContext, CommandTestCE> {

    override fun handleEvent(): PlatformContext.(CommandTestCE) -> Unit {
        return { e ->
            state.result(CommandTestRE("Sample command result: [${e.sig()}] (${e.content})"))
        }

    }
}

class QueryHandlerTest {

    @Test
    fun handleEventTest() {
        val env = procEnv()
        val procCtx = env.procCtxProvider.createProcessingContext()
        val testEvent = QueryTestQE("Sample query")
        val qRes: PairResult<String, String> = QueryTestEventHandler.handleEvent().invoke(procCtx, testEvent)

        assertEquals("queryResult", qRes.first)
        assertEquals("Sample query", qRes.second)
    }
}

class QueryTestQE(val content: String) : QueryEvent()

class PairResult<out A, out B>(val first: A, val second: B) : Serializable, IResultObject {
    override fun toString(): String = "($first, $second)"
}

object QueryTestEventHandler : IQueryHandler<PlatformContext, QueryTestQE, PairResult<String, String>> {
    override fun handleEvent(): PlatformContext.(QueryTestQE) -> PairResult<String, String> {
        return { e -> PairResult("queryResult", e.content) }
    }
}
