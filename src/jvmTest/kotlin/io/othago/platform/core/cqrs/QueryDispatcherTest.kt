/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.cqrs

import io.othago.platform.core.api.cqrs.IResultObject
import io.othago.platform.core.api.cqrs.QueryEvent
import io.othago.platform.core.deploy.PlatformContext
import io.othago.platform.core.deploy.PlatformEnvironment
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import procEnv


class QueryDispatcherTest {

    private lateinit var env: PlatformEnvironment

    @BeforeEach
    fun initTest() {
        env = procEnv()
    }

    @Test
    fun queryDispatcherBaseTest() {
        env.queryHandlerRegistry.registerResource(TestQEH)

        val testQE = TestQueryQE("SampleQuery")
        val res = env.queryDispatcher.dispatchEvent(env.procCtxProvider.createProcessingContext(), testQE)

        Assertions.assertTrue(res is String)
        Assertions.assertEquals("Query Response : SampleQuery", res)
    }

    @Test
    fun queryDirectDispatcherTest() {
        val testQE = TestQueryQE("SampleQuery")
        val res = env.queryDirectDispatcher.dispatchEvent(env.procCtxProvider.createProcessingContext(), testQE, TestQEH)

        Assertions.assertEquals("Query Response : SampleQuery", res)
    }

}

class TestQueryQE(val content: String) : QueryEvent()

class StringResult(val value: String): IResultObject


object TestQEH : IQueryHandler<PlatformContext, TestQueryQE, StringResult> {
    override fun handleEvent(): PlatformContext.(TestQueryQE) -> StringResult {
        return { e -> StringResult("Query Response : ${e.content}") }
    }
}
