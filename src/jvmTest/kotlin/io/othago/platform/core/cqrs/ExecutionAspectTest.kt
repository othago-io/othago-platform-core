/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.cqrs

import io.othago.platform.common.lang.LST
import io.othago.platform.core.api.exception.PlatformException
import io.othago.platform.core.application.ApplicationInstance
import io.othago.platform.core.application.resDscr
import io.othago.platform.core.deploy.*
import io.othago.platform.core.env.DEF_CMD_EP
import io.othago.platform.core.env.IEnvironmentBuilder
import io.othago.platform.core.env.ProcEnvComponentProvider
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import testAppInstance

class ExecutionAspectEnvComponentProvider : ProcEnvComponentProvider<PlatformContext>(
    cmdEntryPointFactory = { pcp, cmdd -> CommandEntryPoint(DEF_CMD_EP, pcp, cmdd, executionAspect = ExceptionHandlingAspect()) } //inject overridden entry point factory
)

class ExecutionAspectEnvironmentBuilder : IEnvironmentBuilder<PlatformContext, PlatformEnvironment> {
    override fun build(appInstance: ApplicationInstance): PlatformEnvironment {
        val procCtxProvider = PlatformContextProvider()
        val env = PlatformEnvironment(procCtxProvider, ExecutionAspectEnvComponentProvider())
        procCtxProvider.injectProcEnv(env)
        return env
    }
}

class ExecutionAspectTest {

    @Test
    fun bootstrapAppTest() {
        val plfAppBuilder = PlatformApplicationBuilder(ExecutionAspectEnvironmentBuilder())
        plfAppBuilder.registerApplicationResources { resDscr(LST(TestCommandP1ExceptionCEH)) }

        val env = plfAppBuilder.createApplication(testAppInstance())

        try {
            env.commandEntryPoint.executeCommand(TestCE())
            fail<Unit>("Exception should be thrown")
        } catch (ex: Throwable) {
            assertTrue(ex is PlatformException)
            assertEquals("Test Exception", (ex as PlatformException).retParam("rootEx"))
            assertEquals("testCE", ex.retParam("actionName"))
        }
    }
}
