/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.audit

import io.othago.platform.core.api.cqrs.DeltaChange
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive
import org.junit.jupiter.api.Test
import kotlin.test.assertTrue

class DeltaTest {
    @Test
    fun flatPropsToNullTest() {
        val objA = JsonObject(mapOf("type" to JsonPrimitive("Abc"), "title" to JsonPrimitive("Test title")))
        val result = objA.delta(null)
        assertTrue("missmatch in delta") {
            result == listOf(
                DeltaChange("type", null, "Abc"),
                DeltaChange("title", null, "Test title")
            )
        }
    }
}