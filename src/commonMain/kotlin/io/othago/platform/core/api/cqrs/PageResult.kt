/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.api.cqrs

import kotlinx.serialization.Serializable

/**
 * Envelop with IResultObject interface and @Serializable
 */
@Serializable
class ListResult<T : IResultObject> : List<T>, IResultObject {
    val list = mutableListOf<T>()
    override val size: Int get() = list.size

    constructor()
    constructor(c: Collection<T>) {
        list.addAll(c)
    }

    override fun contains(element: T): Boolean = list.contains(element)
    override fun containsAll(elements: Collection<T>): Boolean = list.containsAll(elements)
    override fun get(index: Int): T = list[index]
    override fun indexOf(element: T): Int = list.indexOf(element)
    override fun isEmpty(): Boolean = list.isEmpty()
    override fun iterator(): Iterator<T> = list.iterator()
    override fun lastIndexOf(element: T): Int = list.lastIndexOf(element)
    override fun listIterator(): ListIterator<T> = list.listIterator()
    override fun listIterator(index: Int): ListIterator<T> = list.listIterator(index)
    override fun subList(fromIndex: Int, toIndex: Int): List<T> = list.subList(fromIndex, toIndex)
}

fun <E : IResultObject> Collection<E>.toListResult(): ListResult<E> = ListResult(this)

@Serializable
class MapResult<V : IResultObject> : Map<String, V>, IResultObject {
    val map = mutableMapOf<String, V>()
    override val size: Int get() = map.size
    override val entries: Set<Map.Entry<String, V>> get() = map.entries
    override val keys: Set<String> get() = map.keys
    override val values: Collection<V> get() = map.values

    constructor()
    constructor(c: Map<String, V>) {
        map.putAll(c)
    }

    override fun containsKey(key: String): Boolean = map.containsKey(key)
    override fun containsValue(value: V): Boolean = map.containsValue(value)
    override fun get(key: String): V? = map[key]
    override fun isEmpty(): Boolean = map.isEmpty()
}

fun <V : IResultObject> Map<String, V>.toMapResult(): MapResult<V> = MapResult(this)

@Serializable
data class BooleanResult(val value: Boolean) : IResultObject

fun Boolean.toBooleanResult(): BooleanResult = BooleanResult(this)

@Serializable
data class PageableResult(
    val sort: SortResult,
    val offset: Long,
    val pageNumber: Int,
    val pageSize: Int,
    val paged: Boolean,
    val unpaged: Boolean
) : IResultObject

@Serializable
data class SortResult(
    val sorted: Boolean,
    val unsorted: Boolean,
    val empty: Boolean
) : IResultObject

// TODO raw re-implementation from spring.data Page<T>
@Serializable
data class PageResult<T : IResultObject>(
    val content: List<T>,
    val pageable: PageableResult,
    val totalPages: Int?,
    val totalElements: Int?,
    val last: Boolean,
    val size: Int,
    val number: Int,
    val sort: SortResult,
    val first: Boolean,
    val numberOfElements: Int,
    val empty: Boolean
) : IResultObject {
    companion object {
        fun <T : IResultObject> emptyPageResult(): PageResult<T> {
            return PageResult(
                content = listOf(),
                pageable = PageableResult(
                    sort = SortResult(false, false, false),
                    offset = 0,
                    pageNumber = 0,
                    pageSize = 20,
                    paged = true,
                    unpaged = false
                ),
                totalPages = 0,
                totalElements = 0,
                last = true,
                size = 0,
                number = 0,
                sort = SortResult(false, false, false),
                first = true,
                numberOfElements = 0,
                empty = true
            )
        }

        fun <T : IResultObject> listPageResult(list: List<T>): PageResult<T> {
            return PageResult(
                content = list,
                pageable = PageableResult(
                    sort = SortResult(false, false, false),
                    offset = 0,
                    pageNumber = 0,
                    pageSize = list.size,
                    paged = true,
                    unpaged = false
                ),
                totalPages = 0,
                totalElements = list.size,
                last = true,
                size = 0,
                number = 0,
                sort = SortResult(false, false, false),
                first = true,
                numberOfElements = list.size,
                empty = false
            )
        }
    }
}
