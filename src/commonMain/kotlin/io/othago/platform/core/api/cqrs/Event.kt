/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.api.cqrs

import kotlinx.serialization.Polymorphic
import kotlinx.serialization.Serializable
import kotlin.reflect.KClassifier

fun Any.simpleName(): String = this::class.simpleName!!.decapitalize()

interface IPlatformEvent

fun IPlatformEvent.retActionName(): String {
    return this.simpleName()
}

fun IPlatformEvent.retId(): KClassifier {
    return this::class
}

fun IPlatformEvent.sig(): String {
    return "${retActionName()}"
}

/* abstract classes required to resolve serialization issues */
@Serializable
@Polymorphic
abstract class QueryEvent : IPlatformEvent

@Serializable
@Polymorphic
abstract class CommandEvent : IPlatformEvent

@Serializable
@Polymorphic
abstract class RepositoryEvent : IPlatformEvent {
    var executedActionName: String = "UNKNOWN_ACTION" // filled in by frame
}

interface PageableQueryEvent {
    var pageRequest: PageRequest?
}

interface SearchableQueryEvent : PageableQueryEvent {
    var search: String?
}
