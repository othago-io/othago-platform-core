/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.api.validation.constraint

interface Constraint {
    val name: String
}

object NullConstraint : Constraint {
    override val name: String = "NullConstraint"
}

object NotNullConstraint : Constraint {
    override val name: String = "NotNullConstraint"
}

data class EqualsConstraint(val value: Any) : Constraint {
    override val name: String = "EqualsConstraint"
}

data class EqualsIgnoreCaseConstraint(val value: String) : Constraint {
    override val name: String = "EqualsIgnoreCaseConstraint"
}

data class NotEqualsConstraint(val value: Any) : Constraint {
    override val name: String = "NotEqualsConstraint"
}

data class NotEqualsIgnoreCaseConstraint(val value: Any) : Constraint {
    override val name: String = "NotEqualsIgnoreCaseConstraint"
}

data class InConstraint(val values: Collection<*>) : Constraint {
    override val name: String = "InConstraint"
}

data class InIgnoreCaseConstraint(val values: Collection<String>) : Constraint {
    override val name: String = "InConstraint"
}

data class NotInConstraint(val values: Collection<*>) : Constraint {
    override val name: String = "NotInConstraint"
}

data class NotInIgnoreCaseConstraint(val values: Collection<String>) : Constraint {
    override val name: String = "NotInConstraint"
}

object BlankConstraint : Constraint {
    override val name: String = "BlankConstraint"
}

object NotBlankConstraint : Constraint {
    override val name: String = "NotBlankConstraint"
}

data class MatchesConstraint(val pattern: String) : Constraint {
    override val name: String = "MatchesConstraint"
    val regex = pattern.toRegex()

    constructor(regex: Regex) : this(regex.pattern)
}

data class NotMatchConstraint(val pattern: String) : Constraint {
    override val name: String = "NotMatchConstraint"
    val regex = pattern.toRegex()

    constructor(regex: Regex) : this(regex.pattern)
}

data class ContainsRegexConstraint(val pattern: String) : Constraint {
    override val name: String = "ContainsRegexConstraint"
    val regex = pattern.toRegex()

    constructor(regex: Regex) : this(regex.pattern)
}

data class NotContainRegexConstraint(val pattern: String) : Constraint {
    override val name: String = "NotContainRegexConstraint"
    val regex = pattern.toRegex()

    constructor(regex: Regex) : this(regex.pattern)
}

data class StartsWithConstraint(val prefix: String) : Constraint {
    override val name: String = "StartsWithConstraint"
}

data class StartsWithIgnoreCaseConstraint(val prefix: String) : Constraint {
    override val name: String = "StartsWithIgnoreCaseConstraint"
}

data class NotStartWithConstraint(val prefix: String) : Constraint {
    override val name: String = "NotStartWithConstraint"
}

data class NotStartWithIgnoreCaseConstraint(val prefix: String) : Constraint {
    override val name: String = "NotStartWithIgnoreCaseConstraint"
}

data class EndsWithConstraint(val suffix: String) : Constraint {
    override val name: String = "EndsWithConstraint"
}

data class EndsWithIgnoreCaseConstraint(val suffix: String) : Constraint {
    override val name: String = "EndsWithIgnoreCaseConstraint"
}

data class NotEndWithConstraint(val suffix: String) : Constraint {
    override val name: String = "NotEndWithConstraint"
}

data class NotEndWithIgnoreCaseConstraint(val suffix: String) : Constraint {
    override val name: String = "NotEndWithIgnoreCaseConstraint"
}

object EmailConstraint : Constraint {
    override val name: String = "EmailConstraint"
    val regex = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$".toRegex()
}

object WebsiteConstraint : Constraint {
    override val name: String = "WebsiteConstraint"
    val regex = "^(https?:\\/\\/)?([a-zA-Z0-9]+(-?[a-zA-Z0-9])*\\.)+[\\w]{2,}(\\/\\S*)?\$".toRegex()
}

data class LessConstraint(val value: Comparable<*>) : Constraint {
    override val name: String = "LessConstraint"
}

data class LessOrEqualConstraint(val value: Comparable<*>) : Constraint {
    override val name: String = "LessOrEqualConstraint"
}

data class GreaterConstraint(val value: Comparable<*>) : Constraint {
    override val name: String = "GreaterConstraint"
}

data class GreaterOrEqualConstraint(val value: Comparable<*>) : Constraint {
    override val name: String = "GreaterOrEqualConstraint"
}

data class BetweenConstraint(val start: Comparable<*>, val end: Comparable<*>) : Constraint {
    override val name: String = "BetweenConstraint"
}

data class NotBetweenConstraint(val start: Comparable<*>, val end: Comparable<*>) : Constraint {
    override val name: String = "NotBetweenConstraint"
}

object EmptyConstraint : Constraint {
    override val name: String = "EmptyConstraint"
}

object NotEmptyConstraint : Constraint {
    override val name: String = "NotEmptyConstraint"
}

data class ContainsConstraint(val value: Any) : Constraint {
    override val name: String = "ContainsConstraint"
}

data class ContainsIgnoreCaseConstraint(val value: Any) : Constraint {
    override val name: String = "ContainsIgnoreCaseConstraint"
}

data class ContainsAllConstraint(val values: Collection<*>) : Constraint {
    override val name: String = "ContainsAllConstraint"
}

data class ContainsIgnoreCaseAllConstraint(val values: Collection<*>) : Constraint {
    override val name: String = "ContainsIgnoreCaseAllConstraint"
}

data class ContainsAnyConstraint(val values: Collection<*>) : Constraint {
    override val name: String = "ContainsAnyConstraint"
}

data class ContainsIgnoreCaseAnyConstraint(val values: Collection<*>) : Constraint {
    override val name: String = "ContainsIgnoreCaseAnyConstraint"
}

data class NotContainConstraint(val value: Any) : Constraint {
    override val name: String = "NotContainConstraint"
}

data class NotContainIgnoreCaseConstraint(val value: Any) : Constraint {
    override val name: String = "NotContainIgnoreCaseConstraint"
}

data class NotContainAllConstraint(val values: Iterable<*>) : Constraint {
    override val name: String = "NotContainAllConstraint"
}

data class NotContainIgnoreCaseAllConstraint(val values: Iterable<*>) : Constraint {
    override val name: String = "NotContainIgnoreCaseAllConstraint"
}

data class NotContainAnyConstraint(val values: Iterable<*>) : Constraint {
    override val name: String = "NotContainAnyConstraint"
}

data class NotContainIgnoreCaseAnyConstraint(val values: Iterable<*>) : Constraint {
    override val name: String = "NotContainIgnoreCaseAnyConstraint"
}

data class SizeConstraint(val min: Int = Int.MIN_VALUE, val max: Int = Int.MAX_VALUE) : Constraint {
    override val name: String = "SizeConstraint"
}

object UuidConstraint : Constraint {
    override val name: String = "UuidConstraint"
}

// TODO think about them
//object ValidConstraint : Constraint { override val name: String = "ValidConstraint" }
//object LetterConstraint : Constraint { override val name: String = "LetterConstraint" }
//object NotLetterConstraint : Constraint { override val name: String = "NotLetterConstraint" }
//object DigitConstraint : Constraint { override val name: String = "DigitConstraint" }
//object NotDigitConstraint : Constraint { override val name: String = "NotDigitConstraint" }
//object LetterOrDigitConstraint : Constraint { override val name: String = "LetterOrDigitConstraint" }
//object NotLetterOrDigitConstraint : Constraint { override val name: String = "NotLetterOrDigitConstraint" }
//object UpperCaseConstraint : Constraint { override val name: String = "UpperCaseConstraint" }
//object LowerCaseConstraint : Constraint { override val name: String = "LowerCaseConstraint" }

