/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@file:UseSerializers(UuidSerializer::class)

package io.othago.platform.core.api.cqrs

import com.benasher44.uuid.Uuid
import io.othago.platform.core.api.serializers.UuidSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers

// TODO think about identity package

// customization points
interface IObjectType {
    val objectType: String
}

interface IObjectIdentity {
    val objectIdentity: String
}

interface IDataItemEvent : IPlatformEvent {
    val id: Uuid
    val version: Long
}

/* Data Item repository event */
@Serializable
enum class DataItemProto { OBJ, REL }

@Serializable
enum class DataChangeType { ADD, MOD, DEL }

@Serializable
abstract class DataItemRE(
    val dataItemType: String, //entity / data item class
    val changeType: DataChangeType,
    val dataItemProto: DataItemProto = DataItemProto.OBJ //always data object for relational model initially
) : RepositoryEvent(), IDataItemEvent {
    abstract val delta: List<DeltaChange>
}

class GenericDataItemRE(
    override val id: Uuid,
    override val version: Long,
    override val delta: List<DeltaChange>,
    _dataItemType: String,
    _changeType: DataChangeType,
    _dataItemProto: DataItemProto = DataItemProto.OBJ //always data object for relational model initially
) : DataItemRE(_dataItemType, _changeType, _dataItemProto)

@Serializable
data class DeltaChange(
    val fieldName: String,
    val initialValue: String?,
    val updatedValue: String?
)
