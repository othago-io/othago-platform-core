/*
 * Copyright 2015-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.othago.platform.core.api.cqrs

import io.othago.platform.core.api.exception.IPlatformExceptionCode
import kotlinx.serialization.Polymorphic
import kotlinx.serialization.Serializable

@Serializable
abstract class EventResult : IResultObject

@Serializable
data class ErrorResult(val errors: List<ErrorItem> = emptyList()) : EventResult() {
    constructor(errorItem: ErrorItem) : this(listOf(errorItem))
}

@Serializable
data class CommandResult<out R : RepositoryEvent>(val payload: List<@Polymorphic R>) : EventResult()

@Serializable
data class QueryResult<out R : IResultObject>(@Polymorphic val payload: R?) : EventResult()

@Serializable
data class ErrorParam(val key: String, val value: String) : IResultObject

@Serializable
data class ErrorItem(val exCode: ExceptionCode, val traceMsg: String, val params: List<ErrorParam> = emptyList()) : IResultObject

@Serializable //TODO move to validation integration specific api
data class ConstraintViolation(
    // what tested against
    val constraintName: String,
    // where tested
    val dataItemId: String,
    val dataItemType: String,
    // what tested
    val propertyPath: String,
    // what is wrong
    val message: String, //trace message
    val messageBundle: String,
    val messageCode: String,
    val messageParams: Map<String, String> = mutableMapOf(),
    // offending value
    val invalidValue: String?
) {
    fun toErrorParamList(): List<ErrorParam> {
        val res = mutableListOf(
            ErrorParam("constraintName", constraintName),
            ErrorParam("dataItemId", dataItemId),
            ErrorParam("beanItemType", dataItemType),
            ErrorParam("propertyPath", propertyPath),
            //ErrorParam("message",message),  -> trace message, used level up in errorItem struct
            ErrorParam("messageBundle", messageBundle),
            //ErrorParam("messageCode",messageCode.toTransportObject().code), -> used level up in errorItem struct
            ErrorParam("invalidValue", invalidValue ?: "UNKNOWN_VALUE")
        )
        res.addAll(messageParams.map { ErrorParam(it.key, it.value) }) //flatten
        return res
    }
}

// Used for transport
//TODO check whether simplification is possible without spoiling serialization
@Serializable
data class ExceptionCode(override val code: String, override val desc: String) : IPlatformExceptionCode

fun IPlatformExceptionCode.toResultObject(): ExceptionCode = ExceptionCode(code, desc)
